<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('error', [
    'as' => 'error',
    'uses' => 'HomeController@getError'
]);

/** ======================== ADMIN ==========================**/ 

// login
Route::get('admin/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('admin/login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('admin/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

Route::get('admin/home', 'HomeController@index')->name('admin-home');
Route::post('admin/search', 'HomeController@search')->name('admin-search');




Route::prefix('admin/')->group(function () {
    // category
    Route::get('category', [
        'as' => 'category',
        'uses' => 'HomeController@getCate'
    ]);
    Route::get('category/add', [
        'as' => 'get-add-cate',
        'uses' => 'HomeController@getAddCate'
    ]);
    Route::get('category/edit/{id}', [
        'as' => 'edit-cate',
        'uses' => 'HomeController@getEditCate'
    ]);

    Route::post('category/add', [
        'as' => 'post-cate',
        'uses' => 'HomeController@postCate'
    ]);
    
    Route::post('category/edit/{id}', [
        'as' => 'update-cate',
        'uses' => 'HomeController@updateCate'
    ]);




    // slide
    Route::get('slide', [
        'as' => 'slide',
        'uses' => 'HomeController@getSlide'
    ]);
    Route::get('slide/add', [
        'as' => 'get-add-slide',
        'uses' => 'HomeController@getAddSlide'
    ]);
    Route::get('slide/edit/{id}', [
        'as' => 'edit-slide',
        'uses' => 'HomeController@getEditSlide'
    ]);

    Route::post('slide/add', [
        'as' => 'post-slide',
        'uses' => 'HomeController@postSlide'
    ]);

    Route::post('slide/edit/{id}', [
        'as' => 'update-slide',
        'uses' => 'HomeController@updateSlide'
    ]);

    Route::get('delete-slide/{id}', [
        'as' => 'delete-slide',
        'uses' => 'HomeController@deleteSlide'
    ]);

    



    // products
    Route::get('product', [
        'as' => 'product',
        'uses' => 'HomeController@getProduct'
    ]);
    Route::get('product/add', [
        'as' => 'get-add-product',
        'uses' => 'HomeController@getAddProduct'
    ]);
    Route::get('product/edit/{id}', [
        'as' => 'edit-product',
        'uses' => 'HomeController@getEditProduct'
    ]);

    Route::post('product/add', [
        'as' => 'post-product',
        'uses' => 'HomeController@postProduct'
    ]);

    Route::post('product/edit/{id}', [
        'as' => 'update-product',
        'uses' => 'HomeController@updateProduct'
    ]);

    Route::get('getAjax/{type}','HomeController@getAjax')->name('getAjax');
    Route::get('get_ajax_del/{type}','HomeController@getAjaxDel')->name('get_ajax_del');






    // customer
    Route::get('customer', [
        'as' => 'customer',
        'uses' => 'HomeController@getCustomer'
    ]);
    Route::get('customer/add', [
        'as' => 'get-add-customer',
        'uses' => 'HomeController@getAddCustomer'
    ]);

    Route::get('customer/edit/{id}', [
        'as' => 'edit-cus',
        'uses' => 'HomeController@getEditCustomer'
    ]);

    Route::post('customer/add', [
        'as' => 'post-cus',
        'uses' => 'HomeController@postCus'
    ]);

    Route::post('customer/edit/{id}', [
        'as' => 'update-cus',
        'uses' => 'HomeController@updateCus'
    ]);





    // user
     Route::get('user', [
        'as' => 'user',
        'uses' => 'HomeController@getUser'
    ]);
    Route::get('user/add', [
        'as' => 'get-add-user',
        'uses' => 'HomeController@getAddUser'
    ]);

    Route::get('user/edit/{id}', [
        'as' => 'edit-user',
        'uses' => 'HomeController@getEditUser'
    ]);

    Route::post('user/add', [
        'as' => 'post-user',
        'uses' => 'HomeController@postUser'
    ]);

    Route::post('user/edit/{id}', [
        'as' => 'update-user',
        'uses' => 'HomeController@updateUser'
    ]);



    // bill
    Route::get('bill', [
        'as' => 'bill',
        'uses' => 'HomeController@bill'
    ]);

    Route::get('bill/edit/{id}', [
        'as' => 'edit-bill',
        'uses' => 'HomeController@getEditBill'
    ]);


    Route::post('bill/edit/{id}', [
        'as' => 'update-bill',
        'uses' => 'HomeController@updateBill'
    ]);

    Route::get('delete-bill/{id}', [
        'as' => 'delete-bill',
        'uses' => 'HomeController@deleteBill'
    ]);

    Route::get('bill-details/{id}', [
        'as' => 'bill-details',
        'uses' => 'HomeController@billDetails'
    ]);
    
    Route::get('bill/add', [
        'as' => 'get-add-bill',
        'uses' => 'HomeController@getAddBill'
    ]);

    Route::get('add-cart/{id}', [
        'as' => 'add-cart',
        'uses' => 'HomeController@addCart'
    ]);
    
    Route::get('delete-cart/{id}', [
        'as' => 'delete-cart',
        'uses' => 'HomeController@deleteCart'
    ]);

    Route::post('checkout-admin', [
        'as' => 'checkout-admin',
        'uses' => 'HomeController@checkoutAdmin'
    ]);

    Route::post('transform-status/{id}', [
        'as' => 'transform-status',
        'uses' => 'HomeController@transformStatus'
    ]);

    // bill-detail
    Route::get('bill-detail', [
        'as' => 'bill-detail',
        'uses' => 'HomeController@billdetail'
    ]);

    // comment
    Route::get('comment', [
        'as' => 'comment',
        'uses' => 'HomeController@comment'
    ]);

   
    
});













/** ======================== CUSTOMER ==========================**/ 
Route::get('/', [
    'as' => 'index',
    'uses' => 'PageController@getIndex'
]);

// register
Route::get('register', [
    'as' => 'get-customer-register',
    'uses' => 'CustomerController@getRegister'
]);

Route::post('register', [
    'as' => 'post-customer-register',
    'uses' => 'CustomerController@postRegister'
]);

// login
Route::get('login', [
    'as' => 'get-customer-login',
    'uses' => 'CustomerController@getLogin'
]);

Route::post('login', [
    'as' => 'post-customer-login',
    'uses' => 'CustomerController@postLogin'
]);

// logout
Route::get('logout', [
    'as' => 'get-logout',
    'uses' => 'CustomerController@getLogout'
]);

 //home
 Route::get('home', [
    'as' => 'home',
    'uses' => 'PageController@getHome'
]);

 //detail
 Route::get('detail/{id}', [
    'as' => 'detail',
    'uses' => 'PageController@getDetail'
]);

//shop
Route::get('shop/{id}', [
    'as' => 'shop',
    'uses' => 'PageController@getShop'
]);

Route::get('search-shop', [
    'as' => 'search-shop',
    'uses' => 'PageController@searchShop'
]);

Route::get('getpro', [
    'as' => 'getpro',
    'uses' => 'PageController@getpro'
]);

Route::get('search-all', [
    'as' => 'search-all',
    'uses' => 'PageController@searcAll'
]);

Route::get('contact', [
    'as' => 'contact',
    'uses' => 'PageController@contact'
]);

Route::get('about', [
    'as' => 'about',
    'uses' => 'PageController@about'
]);

Route::group(['middleware' => ['customers']], function () {
    // cart
    Route::get('cart', [
        'as' => 'cart',
        'uses' => 'PageController@getCart'
    ]);
    
    Route::get('addCart/{id}', [
        'as' => 'addCart',
        'uses' => 'PageController@addCart'
    ]);
    
    Route::get('del-cart/{id}', [
        'as' => 'del-cart',
        'uses' => 'PageController@delCart'
    ]);
    
    Route::get('update-cart', [
        'as' => 'update-cart',
        'uses' => 'PageController@updateCart'
    ]);
    
    Route::post('checkout', [
        'as' => 'checkout',
        'uses' => 'PageController@checkout'
    ]);

    Route::post('post-cmt', [
        'as' => 'post-cmt',
        'uses' => 'PageController@postCmt'
    ]);
    
    Route::get('cmt', [
        'as' => 'cmt',
        'uses' => 'PageController@getCmt'
    ]);

});

Route::get('generate-pdf/{id}','PageController@generatePDF')->name('generate-pdf');

