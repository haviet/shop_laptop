function changeImage(selector) {
    readURL(selector);
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.image-change').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


