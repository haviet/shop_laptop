<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
	<title>
		hóa đơn
	</title>
	<meta charset="utf-8" />
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
</head>


<body style="font-family: DejaVu Sans;">

    <h2 style="text-align: center">HÓA ĐƠN  </h2>
    <p style="text-align: center">Công ty 3S Intersoft JSC</p>
    <p><b>Mã hóa đơn:</b> {{$id}} </p>
    <p><b>Tên khách hàng: </b>{{$name}} </p>
    <p><b>Số điện thoại: </b>{{$phone}} </p>
    <p><b>Địa chỉ: </b>{{$add}}</b> </p>
    <p><b>Hình thức thanh toán: </b>{{$payment}} </p>
    <p><b>Ngày đặt hàng: </b>{{$date}} </p>
    <p><b>Ghi chú: </b>{{$note}} </p>
    <table border="1" style="width: 100%">
        <thead  >
           <tr>
            <td style="text-align: center"><b>Tên sản phẩm</b></td>
            <td style="text-align: center"><b>Số lượng</b></td>
            <td style="text-align: center"><b>Đơn giá(VNĐ)</b></td>
            <td style="text-align: center"><b>Thành tiền(VNĐ)</b></td>
           </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($bill as $item)
                    @foreach ($arrName as $a)
                        @if ($item->id_product == $a[0])
                            <td>{{$a[1]}}</td>
                            <td style="text-align: center">{{$item->quantity}}</td>
                            <td style="text-align: center">{{number_format($item->total_price/$item->quantity)}}</td>
                            <td style="text-align: center">{{number_format($item->total_price)}}</td>
                        @endif
                    @endforeach
                @endforeach
            
            </tr>
        </tbody>
    </table>
       

    <b>Tổng đơn: {{$total}} VNĐ</b>
    <p>Cảm ơn quý khách!</p>
</body> 
</html>
