@extends('admin/master')
@section('title')
    customer
@endsection
@section('content')
<style>
    th, td {
        border: 1px solid #ccc;
    }
</style>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    Danh sách khách hàng
                            </h3>
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">

                            <table id="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Mã khách hàng</th>
                                        <th>Tên</th>
                                        <th>Email</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
    </div>
    
@endsection

@section('script')
    

<script>
    var oTable= $('#table').DataTable({
        "scrollX": false,
        dom: 'lifrtp',
        processing: true,
        serverSide: true,
        orderable: false,
        searchable: false,
        bFilter: false,
        searching: true,
        language: {
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }          
        },
        ajax: {
            url: "{!! route('getAjax', ['type' => 'cus']) !!}",
        },
        columns: [
            { data: 'id',name:'id' },
            { data: 'username',name:'username' },
            { data: 'email',name:'email' },
            { data: 'manipulation',name:'manipulation',class:'manipulation' },
        
        ],
        initComplete: function(){
            $('a[id^=del-cus-]').click(function(e) {
            e.preventDefault();
            if(confirm("Bạn có chắc chắn muốn xóa không?")) {
                var delproid =$(this).attr('id').slice(8);
                var a=$(this).attr('id');
            
                var data={
                    'id':delproid
                };
                console.log(data)

                var url= "{!! route('get_ajax_del', ['type' => 'cus']) !!}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    success: function(res, status, jqXHR) {
                        if(res.status) { 
                            $('#'+a).parents('tr').remove();
                        }
                    },
                });
                }
            })
        },
    
        "lengthMenu": [[10,20,50,100], [10,20,50,100]],
        'order': [[0, 'desc']]
    });
    
</script>
@endsection