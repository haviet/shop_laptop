@extends('admin/master')
@section('title')
    add product
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-7">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                           Thêm mới sản phẩm
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <form  enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right" action="{{route('post-product')}}" method='post'>
                                {{ csrf_field() }}
                                @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group">
                                        <label>
                                            Mã sản phẩm
                                        </label>
                                        <input name="productCode" type="text" class="form-control m-input m-input--square" >
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>
                                            Tên sản phẩm
                                        </label>
                                        <input name="name" type="text" class="form-control m-input m-input--square" >
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>
                                            Danh mục
                                        </label>
                                        <select name="id_cate" class="form-control m-input m-input--square">
                                            @foreach ($cate as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="">
                                            Hình ảnh
                                        </label>
                                        <input onchange="changeImage(this)" name="image" type="file" class="form-control m-input m-input--square">
                                        <img class="image-change" src="" />
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="">
                                            Giá sản phẩm
                                        </label>
                                        <input min="10000" value="10000" name="unit_price" type="number" class="form-control m-input m-input--square"  placeholder="Enter unit price">
                                       
                                    </div>

                                    {{-- <div class="form-group m-form__group">
                                        <label for="">
                                           Giá khuyến mãi
                                        </label>
                                        <input min="0" value="0" name="promotion_price" type="number" class="form-control m-input m-input--square"  placeholder="Enter promotion price">
                                    </div> --}}

                                    <div class="form-group m-form__group">
                                        <label for="">
                                            Số lượng
                                        </label>
                                        <input min="0" value="0" name="quantity" type="number" class="form-control m-input m-input--square"  placeholder="Enter quantity">
                                       
                                    </div>

                                    <div class="form-group m-form__group">
                                        <label for="">
                                            Kích hoạt
                                        </label>
                                        <select name="active" class="form-control m-input m-input--square" id="exampleSelect1">
                                            <option>
                                                Active
                                            </option>
                                            <option>
                                                Inactive
                                            </option>
                                            
                                        </select>
                                    </div>

                                    <div class="form-group m-form__group">
                                        <label for="">
                                            Mô tả
                                        </label>
                                        <textarea name="description" id="description"></textarea>
                                        <script>CKEDITOR.replace('description');</script>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button class="btn btn-metal" type="submit" name="action" value="add">
                                            Thêm
                                        </button>
                                        <button class="btn btn-secondary" type="submit" name="action" value="cancel">
                                            Hủy
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>



    
@endsection