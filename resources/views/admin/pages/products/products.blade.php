@extends('admin/master')
@section('title')
    product
@endsection
@section('content')
<style>
    th, td {
        border: 1px solid #ccc;
    }
</style>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    Danh sách sản phẩm
                            </h3>   
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">

                            <table id="table" style="width:100%">
                                <thead>
                                    <tr>
                                        {{-- <th>id</th> --}}
                                        <th>Mã SP</th>
                                        <th>Tên</th>
                                        <th>Hình ảnh</th>
                                        <th>Đơn giá(VND)</th>
                                        <th>Số lượng</th>
                                        <th>Kích hoạt</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
    </div>
    
@endsection

@section('script')


    {{-- <script>
        document.getElementById('search').addEventListener('keyup', function(e) {
            if(e.keyCode === 13) {
                e.preventDefault();
            }
            var url='{!!route("search")!!}';
            var data=this.value;
            axios.get(url, {
                params: {
                    key:data
                }
            })
            .then(function (res) {
                var html = '';
                var stt = 1;
                
                for(var i = 0; i< res.data.product.length; i++) {
                    var pro = res.data.product[i]; 
                    console.log(pro);
                      
                    html+='<tr height="50px">';
                    html+='    <th scope="row">'+ stt++ +'</th>';
                    for(var j =0; j<res.data.cate.length; j++) {
                        if(res.data.cate[j].id === res.data.product[i].id_cate) {
                            html+='<td>'+res.data.cate[j].name+'</td>';
                        }
                    }
                    html+='    <td>'+pro.name+'</td>';
                    html+='    <td>';
                    html+='        <img src="/upload/product/'+pro.image+'" alt="">';
                    html+='    </td>';
                    html+='    <td class="desc">'+pro.description+'</td>';
                    html+='    <td>'+pro.unit_price+'</td>';
                    html+='    <td>'+pro.promotion_price+'</td>';
                    html+='    <th>'+pro.quantity+'</th>';
                    html+='    <td>'+pro.active+'</td>';
                    html+='    <td>';
                    html+='        <a href="{{route("edit-product",$item->id )}}" class="btn btn-brand m-btn btn-sm 	m-btn m-btn--icon">';
                    html+='            <span><i class="fas fa-pencil-alt"></i><span>edit</span></span>';
                    html+='        </a>';
                    html+='    </td>';
                    html+='    <td>';
                    html+='        <a onclick="return confirm("Are you sure?")" href="{{ route("delete-product", $item->id)}}" class="btn btn-danger m-btn btn-sm 	m-btn m-btn--icon">';
                    html+='            <span><i class="fas fa-trash-alt"></i> <span> Delete</span> </span>';
                    html+='        </a>';
                    html+='    </td>';
                    html+='</tr>';
                    
                }   
                document.getElementById('changeTable').innerHTML = html;          
            })
            .catch(function (error) {
                console.log(error);
            });
            
        })
        $('.btn-search').click(function(e) {
            e.preventDefault();
            
        });
    </script> --}}
    

<script>
    var oTable= $('#table').DataTable({
        "scrollX": false,
        dom: 'lifrtp',
        processing: true,
        serverSide: true,
        orderable: false,
        searchable: false,
        bFilter: false,
        searching: true,
        language: {
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }          
        },
        ajax: {
            url: "{!! route('getAjax', ['type' => 'products']) !!}",
        },
        columns: [
            // { data: 'id',name:'id' },
            { data: 'productCode',name:'productCode' },
            { data: 'name',name:'name' },
            { data: 'image',name:'image' },
            { data: 'unit_price',name:'unit_price' },
            { data: 'quantity',name:'quantity' },
            { data: 'active',name:'active' },
            { data: 'manipulation',name:'manipulation',class:'manipulation' },
        
        ],
        initComplete: function(){
            $('a[id^=del-pro-]').click(function(e) {
            e.preventDefault();
            if(confirm("Bạn có chắc chắn muốn xóa không?")) {
                var delproid =$(this).attr('id').slice(8);
                var a=$(this).attr('id');
            
                var data={
                    'id':delproid
                };
                var url= "{!! route('get_ajax_del', ['type' => 'products']) !!}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    success: function(res, status, jqXHR) {
                        if(res.status) { 
                            $('#'+a).parents('tr').remove();
                        }
                    },
                });
                }
            })
        },
    
        "lengthMenu": [[10,20,50,100], [10,20,50,100]],
        'order': [[0, 'desc']]
    });
    
</script>
@endsection