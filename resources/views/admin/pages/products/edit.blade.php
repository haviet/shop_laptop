@extends('admin/master')
@section('title')
    edit product
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-7">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                            Chỉnh sửa thông tin sản phẩm
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{route('update-product', $pro->id)}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Tên sản phẩm
                                        </label>
                                        <input value="{{$pro->name}}" name="name" type="text" class="form-control m-input m-input--square" >
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>
                                            Mã sản phẩm
                                        </label>
                                        <input value="{{$pro->productCode}}" name="productCode" type="text" class="form-control m-input m-input--square" >
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleSelect1">
                                                Danh mục
                                        </label>
                                        <select name="id_cate" class="form-control m-input m-input--square">
                                            @foreach ($cate as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>  
                                                @if ($item->id === $pro->id_cate)
                                                    <option style="display:none;" selected value="{{$item->id}}">{{$item->name}}</option>  
                                                @endif
                                            @endforeach
                                        </select>
                                       
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Hình ảnh
                                        </label>
                                        <input onchange="changeImage(this)" name="image" type="file" class="form-control m-input m-input--square">
                                        <img class="image-change" src="/upload/product/{{$pro->image}}" />
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Giá sản phẩm
                                        </label>
                                        <input min="10000"  value="{{$pro->unit_price}}" name="unit_price" type="number" class="form-control m-input m-input--square"  placeholder="Enter unit price">
                                       
                                    </div>

                                    {{-- <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Giá khuyến mãi
                                        </label>
                                        <input min="0" value="{{$pro->promotion_price}}" name="promotion_price" type="number" class="form-control m-input m-input--square"  placeholder="Enter promotion price">
                                    </div> --}}

                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Số lượng
                                        </label>
                                        <input min="0" value="{{$pro->quantity}}" name="quantity" type="number" class="form-control m-input m-input--square"  placeholder="Enter quantity">
                                       
                                    </div>

                                    <div class="form-group m-form__group" >
                                        <label for="exampleSelect1">
                                            Kích hoạt
                                        </label>
                                       
                                        <select name="active" class="form-control m-input m-input--square" id="exampleSelect1">
                                            @if ($pro->active)
                                                <option style="display:none;" selected>{{$pro->active}}</option>  
                                            @endif
                                            <option>
                                                Active
                                            </option>
                                            <option>
                                                Inactive
                                            </option>
                                            
                                        </select>
                                    </div>

                                    <div class="form-group m-form__group" id="form-desc" >
                                        <label for="exampleInputEmail1">
                                            Mô tả
                                        </label>
                                        <textarea value='' name="description" id="description">{!! $pro->description !!}</textarea>
                                        <script>CKEDITOR.replace('description')</script>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button name="action" class="btn btn-secondary" value="cancel">
                                            Hủy
                                        </button>
                                        <button name="action" class="btn btn-metal" value="save">
                                            Cập nhật
                                        </button>
                                        
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
        <!-- END: Subheader -->
    </div>  
@endsection
