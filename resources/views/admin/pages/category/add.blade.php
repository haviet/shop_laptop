@extends('admin/master')
@section('title')
    add category
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-7">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon m--hide">
                                            <i class="la la-gear"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">
                                           Thêm mới danh mục
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{route('post-cate')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
        
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Tên danh mục
                                        </label>
                                        <input name="name" type="text" class="form-control m-input m-input--square"  placeholder="Enter name">
                                       
                                    </div>
                                    
                                    <div class="form-group m-form__group">
                                        <label>
                                            Kích hoạt
                                        </label>
                                        <select name="active" class="form-control m-input m-input--square" id="exampleSelect1">
                                            <option>
                                                Active
                                            </option>
                                            <option>
                                                Inactive
                                            </option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button name="action" type="submit" class="btn btn-metal" value="add"> 
                                            Thêm
                                        </button>
                                        <button name="action" type="submit" class="btn btn-secondary" value="cancel">
                                            Hủy
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>



    
@endsection