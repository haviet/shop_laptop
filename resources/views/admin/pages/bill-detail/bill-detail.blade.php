@extends('admin/master')
@section('title')
    bill detail
@endsection
@section('content')
<style>
    th, td {
        border: 1px solid #ccc;
    }
</style>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    Danh sách chi tiết hóa đơn
                            </h3>
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">

                            <table id="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Mã hóa đơn</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>Đơn giá(VND)</th>
                                        <th>Tổng giá(VND)</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
    </div>
    
@endsection

@section('script')
    

<script>
    var oTable= $('#table').DataTable({
        "scrollX": false,
        dom: 'lifrtp',
        processing: true,
        serverSide: true,
        orderable: false,
        searchable: false,
        bFilter: false,
        searching: true,
        language: {
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }          
        },
        ajax: {
            url: "{!! route('getAjax', ['type' => 'detail']) !!}",
        },
        columns: [
            { data: 'id_bill',name:'id_bill' },
            { data: 'id_product',name:'id_product' },
            { data: 'name_pro',name:'name_pro' },
            { data: 'quantity',name:'quantity' },
            { data: 'unit_price',name:'unit_price' },
            { data: 'total_price',name:'total_price' },
        
        ],
        initComplete: function(){
           
        },
    
        "lengthMenu": [[10,20,50,100], [10,20,50,100]],
        'order': [[0, 'desc']]
    });
    
</script>
@endsection