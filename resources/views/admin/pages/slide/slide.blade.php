@extends('admin/master')
@section('title')
    slide
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    Danh sách hình ảnh quảng cáo
                            </h3>
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            
                            
                            <div class="count-item">
                                Đang xem <strong>{{ $slide -> count()}}</strong> chỉ mục trong tổng số <strong>{{ $slide_count}} </strong>chỉ mục
                            </div>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Hình Ảnh</th>
                                        <th>Link</th>
                                        <th>Kích hoạt</th>
                                        <th> </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($slide as $item)
                                        <tr>
                                            <th scope="row">{{$stt++}}</th>
                                            <td>
                                                <img src="/upload/slide/{{$item->image}}" alt="">
                                            </td>
                                            <td><a href="#">{{$item->link}}</a></td>
                                            <td>{{$item->active}}</td>
                                            <td>
                                                <a href="{{route('edit-slide',$item->id)}}" class="btn btn-brand m-btn btn-sm m-btn m-btn--icon">
                                                    <span><i class="fas fa-pencil-alt"></i><span>edit</span></span>
                                                </a>
                                            </td>
                                            <td>
                                                <a onclick="return confirm('Are you sure?')" href="{{route('delete-slide',$item->id)}}" class="btn btn-danger m-btn btn-sm 	m-btn m-btn--icon">
                                                    <span><i class="fas fa-trash-alt"></i> <span> Delete</span> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                            <div>{{ $slide -> links()}}</div>
                            
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>
@endsection
@section('script')

@endsection
