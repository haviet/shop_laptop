@extends('admin/master')
@section('title')
    add bill
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                 Tạo hóa đơn
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <div class="row">
                                                <div class="col-md-7" style="border-right: 1px solid #ccc;
                                                padding-right: 20px;">
                                                    <div class="m-section">
                                                        <div class="m-section__content">
                                                            <div class="title_left">
                                                                <h3>Chọn sản phẩm cần thêm vào đơn hàng</h3>
                                                                <p>Đã thêm {{$count}} sản phẩm</p>
                                                            </div>
                                                            <table id="table" style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        {{-- <th>id</th> --}}
                                                                        <th>Mã SP</th>
                                                                        <th>Tên</th>
                                                                        {{-- <th>Ảnh</th> --}}
                                                                        <th>Giá(VND)</th>
                                                                        <th>SL</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                                                        <div class="panel-body">
                                                            @if (\Session::has('mess'))
                                                                <div class="alert alert-success">
                                                                    <ul>
                                                                        <li>{!! \Session::get('mess') !!}</li>
                                                                        <a style="font-size: 22px" href="{{route('generate-pdf',Session::get('id'))}}">In hóa đơn</a>
                                                                    </ul>
                                                                </div> 
                                                            @endif
                                                            @if ($count == 0)  
                                                                <h6>Chọn sản phẩm trước khi tạo hóa đơn</h6>
                                                            @else
                                                        
                                                            <form action="{{route('checkout-admin')}}" method="POST" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                @foreach ($errors->all() as $error)
                                                                    <div class="col-lg-12 col-xl-12 alert alert-danger">{{ $error }}</div>
                                                                @endforeach
                                                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                                <input type="hidden" name="employeeCode" value={{ Auth::user()->id }} />

                                                                <div class="form-group">
                                                                    <label>Tên khách hàng</label>
                                                                    <input type="text" class="form-control" name="name" >
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input type="text" class="form-control" name="email" >
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Giới tính</label>
                                                                    <select name="gender" class="form-control">
                                                                        <option value="1">Nữ</option>
                                                                        <option value="0">Nam</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Số điện thoại</label>
                                                                    <input type="number" class="form-control" name="phone">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Địa chỉ</label>
                                                                    <input type="text" class="form-control" name="address">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Tên nhân viên</label>
                                                                    <input readonly type="text" class="form-control" name="employeeName" value={{ Auth::user()->name }}>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Sản phẩm</label> <br>
                                                                    <table border="1" style="width: 100%;">
                                                                        <thead>
                                                                            <th>Tên</th>
                                                                            <th>SL</th>
                                                                            <th>Giá (VND)</th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($cart as $item)
                                                                             <tr>
                                                                                <td>{{$item->name}}</td>
                                                                                <td>
                                                                                    @foreach ($product as $p)
                                                                                        @if ($p->id == $item->id)
                                                                                            <input id-tr='{{$item->rowId}}' class="changeQuantity" type="number" min="1" max='{{$p->quantity}}' value="{{$item->qty}}" style="width: 100%;border: none;">
                                                                                        @endif
                                                                                    @endforeach
                                                                                </td>
                                                                                <td >{{number_format($item->qty*$item->price)}}
                                                                                    </td>
                                                                               
                                                                                <td>
                                                                                    <a  href="{{route('delete-cart', $item->rowId)}}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                                                                                        <i class="fas fa-trash-alt"></i>
                                                                                    </a>
                                                                                </td>
                                                                                <td id-tr='{{$item->rowId}}' style="cursor: pointer" class="update-cart">
                                                                                    <a  class=" .update-cart m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Cập nhật">
                                                                                        <i class="fas fa-pen-alt"></i>
                                                                                    </a>
                                                                                </td>
                                                                             </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                        
                                                                <div class="form-group">
                                                                    <label>Tổng cộng: </label>
                                                                    <input class="total" name="total" value="{{number_format(str_replace(',', '',$total))}} đồng" type="text" style="background:none;display: initial;border: none; outline: none;">
                                                                </div>
                                        
                                                                <div class="form-group">
                                                                    <label>Ngày đặt hàng</label>
                                                                    <input readonly value="{{$date}}" type="date" class="form-control" name="date_order">
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label>Hình thức thanh toán</label>
                                                                    <input readonly type="text" class="form-control" name="payment" value="Thanh toán trực tiếp">
                                                                </div>
                                                               
                                                                <div class="form-group login-submit">
                                                                    <p>Vui lòng CẬP NHẬT giỏ hàng trước khi tạo hóa đơn</p>
                                                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Tạo hóa đơn</button>
                                                                </div>
                                        
                                                            </form>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>
@endsection

@section('script')
    <script>
         var oTable= $('#table').DataTable({
            "scrollX": false,
            dom: 'lifrtp',
            processing: true,
            serverSide: true,
            orderable: false,
            searchable: false,
            bFilter: false,
            searching: true,
            language: {
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix":  "",
                "sSearch":       "Tìm:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Đầu",
                    "sPrevious": "Trước",
                    "sNext":     "Tiếp",
                    "sLast":     "Cuối"
                }          
            },
            ajax: {
                url: "{!! route('getAjax', ['type' => 'createBill']) !!}",
            },
            columns: [
                // { data: 'id',name:'id' },
                { data: 'productCode',name:'productCode' },
                { data: 'name',name:'name' },
                // { data: 'image',name:'image' },
                { data: 'unit_price',name:'unit_price' },
                { data: 'quantity',name:'quantity' },
                { data: 'manipulation',name:'manipulation',class:'manipulation' },
            
            ],
            initComplete: function(){
                $('a[id^=del-pro-]').click(function(e) {
                e.preventDefault();
                if(confirm("Bạn có chắc chắn muốn xóa không?")) {
                    var delproid =$(this).attr('id').slice(8);
                    var a=$(this).attr('id');
                
                    var data={
                        'id':delproid
                    };
                    var url= "{!! route('get_ajax_del', ['type' => 'products']) !!}";
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        dataType: 'json',
                        beforeSend: function() {
                        },
                        success: function(res, status, jqXHR) {
                            if(res.status) { 
                                $('#'+a).parents('tr').remove();
                            }
                        },
                    });
                    }
                })
            },
        
            "lengthMenu": [[10,20,50,100], [10,20,50,100]],
            'order': [[0, 'desc']]
        });








        $('.total').on('keydown keypress', function (event) {
            event.preventDefault();
        });

        $(document).ready(function(){
            $('.update-cart').click(function(){
                var rowId =  $(this).attr('id-tr');
                var quantity = this.parentElement.querySelector('.changeQuantity').value;                
                var url='{!!route("update-cart")!!}';
                var max =  this.parentElement.querySelector('.changeQuantity').getAttribute('max');
                if(quantity > parseInt(max)) {
                    alert('Trong kho chỉ còn lại '+max+' sản phẩm');
                } else {
                    var data = {
                        'rowId': rowId,
                        'qty': quantity,
                    };
                    $.ajax({
                        type: "get",
                        url : url,    
                        data: data,
                        dataType: 'json',
                        success: function(res) {
                            if(res.status){
                                location.reload();
                            }
                        },
                    });
                }  
           }) 
        })   
    </script>
@endsection