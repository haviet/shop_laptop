@extends('admin/master')
@section('title')
    bill-detail
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                 Chi tiết hóa đơn
                            </h3>
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Mã hóa đơn</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>Tổng giá(VND)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($bill_detail as $item)
                                        <tr>
                                            <th scope="row">
                                                {{$stt++}}
                                            </th>
                                            <td>
                                                {{$item->id_bill}}
                                            </td>
                                            @foreach ($pro as $p)
                                            
                                                @if ($item->id_product === $p->id)
                                                <td>
                                                    {{$p->productCode}}
                                                </td>
                                                <td>
                                                    {{$p->name}}
                                                </td>
                                                @endif
                                            @endforeach
                                            <td>
                                                {{$item->quantity}}
                                            </td>
                                            <td>
                                                {{number_format($item->total_price)}}
                                                
                                            </td>
                                           
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>
@endsection