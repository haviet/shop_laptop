@extends('admin/master')
@section('title')
    bill
@endsection
@section('content')
<style>
    th, td {
        border: 1px solid #ccc;
    }
</style>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    Danh sách hóa đơn
                            </h3>
                        </div>
                    </div>
                </div>
                @if (Session::has('success'))
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                @endif
                @if (Session::has('noti'))
                    <div class="alert alert-danger">{{ Session::get('noti') }}</div>
                @endif
                <p style="margin: 25px 0px 1px 32px;font-size: 18px;">Chọn mã hóa đơn:
                    <input type="number" id="print-bill" style="width: 90px;padding: 0px 5px;">
                    <a id="hihi" href="">In đơn hàng</a>
                </p>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">

                            <table id="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Mã hóa đơn</th>
                                        <th>Mã KH</th>
                                        <th>Tên KH</th>
                                        <th>Mã nhân viên</th>
                                        <th>Tên nhân viên</th>
                                        <th>SĐT</th>
                                        <th>Địa chỉ</th>
                                        <th>Ngày đặt hàng</th>
                                        <th>Tổng tiền (VND)</th>
                                        <th>Hình thức thanh toán</th>
                                        <th>Trạng thái</th>
                                        <th>Ghi chú</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
    </div>
    
@endsection

@section('script')
    

<script>
    $('#print-bill').on('keyup', function(){
        $('#hihi').attr('href', '/generate-pdf/'+this.value) 
    })
    var oTable= $('#table').DataTable({
        "scrollX": false,
        dom: 'lifrtp',
        processing: true,
        serverSide: true,
        orderable: false,
        searchable: false,
        bFilter: false,
        searching: true,
        language: {
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }          
        },
        ajax: {
            url: "{!! route('getAjax', ['type' => 'bills']) !!}",
        },
        columns: [
            { data: 'id',name:'id' },
            { data: 'id_customer',name:'id_customer' },
            { data: 'username',name:'username' },
            { data: 'employeeCode',name:'employeeCode' },
            { data: 'employeeName',name:'employeeName' },
            { data: 'phone',name:'phone' },
            { data: 'address',name:'address' },
            { data: 'date_order',name:'date_order' },
            { data: 'total',name:'total' },
            { data: 'payment',name:'payment' },
            { data: 'status',name:'status' },
            { data: 'note',name:'note' },
            { data: 'manipulation',name:'manipulation',class:'manipulation' },
        
        ],
        initComplete: function(){
            $('a[id^=del-bill-]').click(function(e) {
            e.preventDefault();
            if(confirm("Bạn có chắc chắn muốn xóa không?")) {
                var delproid =$(this).attr('id').slice(9);
                var a=$(this).attr('id');
            
                var data={
                    'id':delproid
                };
                var url= "{!! route('get_ajax_del', ['type' => 'bills']) !!}";
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                    },
                    success: function(res, status, jqXHR) {
                        if(res.status) { 
                            $('#'+a).parents('tr').remove();
                        }
                    },
                });
                }
            })
        },
    
        "lengthMenu": [[10,20,50,100], [10,20,50,100]],
        'order': [[0, 'desc']]
    });
    
</script>
@endsection