@extends('admin/master')


@section('title')
    home
@endsection
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Thống kê các sản phẩm đã bán ra
                            </h3>
                        </div>
                    </div>
                </div>
               
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="row">  
                            <div class="col-8">
                                <form action="{{route('admin-search')}}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="date" name="startDate">
                                    <input type="date" name="endDate">
                                    <button>Xem thống kê</button>
                                    <h5 style="margin-top: 10px">Tổng doanh thu: {{number_format($total)}} VND</h5>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Id sản phẩm</th>
                                                <th>Mã sản phẩm</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Số lượng</th>
                                                <th>Tổng giá(VND)</th>
    
                                            </tr>
                                        </thead>
                                        <tbody>                                
                                            @foreach ($result as $item)
                                                <tr>
                                                    <th scope="row">
                                                        {{$stt++}}
                                                    </th>
                                                    <td>
                                                        {{$item->id_product}}
                                                    </td>
                                                    <td>
                                                        {{$item->productCode}}
                                                    </td>
                                                    
                                                    <td>
                                                        {{$item->name}}
                                                    </td>
                                                    <td>
                                                        {{$item->quantity}}
                                                    </td>
                                                    <td>
                                                        {{number_format($item->total_price)}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                            <h6 style="font-weight: bold; font-size: 16px;">Tổng mã sản phẩm</h6>
                                            <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                                    {{$pro}} mã sản phẩm
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                            <h6 style="font-weight: bold; font-size: 16px;">Tổng sản phẩm đã bán</h6>
                                            <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                                    {{$sum2}} sản phẩm
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-12">
                                        <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                            <h6 style="font-weight: bold; font-size: 16px;">Tổng sản phẩm còn lại trong kho</h6>
                                            <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                                    {{$store}} sản phẩm
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                            <h6 style="font-weight: bold; font-size: 16px;">Tổng số hóa đơn</h6>
                                            <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                                    {{$bill}} hóa đơn
                                            </p>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="m-section__content" style="margin-bottom: 41px;padding: 10px;text-align: center;border: 1px solid #ccc;box-shadow: 3px 4px 5px #ccc;">
                                            <h6 style="font-weight: bold; font-size: 16px;">Tổng số khách hàng</h6>
                                            <p style="font-size: 27px; margin-bottom: 0px; color: #1d2dad;">
                                                    {{$cus}} khách hàng
                                            </p>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
                
        </div>
        <!-- END: Subheader -->
       
        

    </div>
@endsection

