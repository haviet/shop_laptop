<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
        <!-- BEGIN: Aside Menu -->
        <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                @if ( Auth::user()->position == "Quản trị viên")
                <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                    <a  href="{{route('admin-home')}}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Thống kê
                                </span>
                                
                            </span>
                        </span>
                    </a>
                </li>
                @endif
               
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Danh mục quản lý
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                @if ( Auth::user()->position == "Nhân viên")
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý đơn hàng
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('bill')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả đơn hàng
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-bill')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới đơn hàng
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a href="{{route('comment')}}" class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý bình luận
                        </span>
                    </a>
                </li>
            
                @else 
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý quảng cáo
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-slide')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới quảng cáo
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('slide')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả quảng cáo
                                    </span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-share"></i>
                        <span class="m-menu__link-text">
                            Quản lý danh mục
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-cate')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới danh mục
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('category')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả danh mục
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-multimedia-1"></i>
                        <span class="m-menu__link-text">
                            Quản lý sản phẩm
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-product')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới sản phẩm
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('product')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả sản phẩm
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-interface-1"></i>
                        <span class="m-menu__link-text">
                            Quản lý người dùng
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-user')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới người dùng
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('user')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả người dùng
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý đơn hàng
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('bill')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả đơn hàng
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-bill')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới đơn hàng
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý chi tiết đơn hàng
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('bill-detail')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả đơn hàng chi tiết
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-interface-6"></i>
                        <span class="m-menu__link-text">
                            Quản lý khách hàng
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('get-add-customer')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Thêm mới khách hàng
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true" >
                                <a  href="{{route('customer')}}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">
                                        Tất cả khách hàng
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                    <a href="{{route('comment')}}" class="menu m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers"></i>
                        <span class="m-menu__link-text">
                            Quản lý bình luận
                        </span>
                    </a>
                </li>
                @endif
                    
                
                
            </ul>
        </div>
        <!-- END: Aside Menu -->
    </div>

@section('script')
<script>
        $(document).ready(function() {
            console.log($('.menu'));
            
        })
        
    </script>
@endsection
