
@extends('customer/form')
@section('title')
    Register
@endsection


@section('content')
<div class="m-stack m-stack--hor m-stack--desktop">
    <div class="m-stack__item m-stack__item--fluid">
        <div class="m-login__wrapper">
            <div class="m-login__logo">
                <a href="#">
                    <img src="assets/app/media/img//logos/logo-2.png">
                </a>
            </div>
            
            <!-- content -->
            <div class="m-login__signin">
                <div class="m-login__head">
                    <h3 class="m-login__title">
                        Đăng kí tài khoản
                    </h3>
                </div>
               
                <form class="m-login__form m-form" action="{{route('post-customer-register')}}" method="post">
                    {{ csrf_field()}}
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">{{ $error }}</div>
                    @endforeach
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input" type="text" placeholder="Tên đăng nhập" name="username" autocomplete="off">
                    </div>
            
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input" type="email" placeholder="Email" name="email" autocomplete="off">
                    </div>
            
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input " type="password" placeholder="Mật khẩu" name="password">
                    </div>
            
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input " type="password" placeholder="Nhập lại mật khẩu" name="re_password">
                    </div>
            
                    <div class="m-login__form-action">
                        <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                            Đăng kí
                        </button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    <div class="m-stack__item m-stack__item--center">
        <div class="m-login__account">
            <span class="m-login__account-msg">
                Bạn đã có tài khoản...
            </span>
            &nbsp;&nbsp;
            <a href="{{route('get-customer-login')}}" class="m-link m-link--focus m-login__account-link">
                Đăng nhập
            </a>
        </div>
    </div>
</div>

@endsection

