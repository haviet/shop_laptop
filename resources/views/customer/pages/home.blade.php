@extends('customer/master')
@section('title')
    home
@endsection
@section('content')
    <!-- Slider -->
    <section class="section-slide">
		<div class="wrap-slick1">
			<div class="slick1">
                @foreach ($slide as $item)
            
                 @if ($item->active === "Active")
                    <div class="item-slick1" style="background-image: url(/upload/slide/{{$item->image}});">
                        <img style="width: 1349px;" src='/upload/slide/{{$item->image}}' />
                    </div>  
                 @endif 
				@endforeach
			</div>
		</div>
	</section>
    <!-- Banner -->
	<div class="sec-banner bg0 p-t-80 p-b-50">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xl-4 p-b-30 m-lr-auto">
                    <!-- Block1 -->
                    <div class="block1 wrap-pic-w" style="padding-top: 109px;">
                        <img src="/upload/product/dell7.jpg" alt="IMG-BANNER" style="height: 300px;">

                        <a href="{{route('getpro')}}" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                            <div class="block1-txt-child1 flex-col-l">
                                <span class="block1-name ltext-102 trans-04 p-b-8">
                                    Macbook Pro
                                </span>

                                <span class="block1-info stext-102 trans-04">
                                        New 2019
                                </span>
                            </div>

                            <div class="block1-txt-child2 p-b-4 trans-05">
                                <div class="block1-link stext-101 cl0 trans-09">
                                    Shop Now
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-b-30 m-lr-auto">
                    <!-- Block1 -->
                    <div class="block1 wrap-pic-w" style="padding-top: 109px;">
                        <img src="/upload/product/as5.jpg" alt="IMG-BANNER" style="height: 300px;">

                        <a href="{{route('getpro')}}" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                            <div class="block1-txt-child1 flex-col-l">
                                <span class="block1-name ltext-102 trans-04 p-b-8">
                                    Lapptop Dell
                                </span>

                                <span class="block1-info stext-102 trans-04">
                                    New 2019
                                </span>
                            </div>

                            <div class="block1-txt-child2 p-b-4 trans-05">
                                <div class="block1-link stext-101 cl0 trans-09">
                                    Shop Now
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-xl-4 p-b-30 m-lr-auto" >
                    <!-- Block1 -->
                    <div class="block1 wrap-pic-w" style="padding-top: 109px;">
                        <img src="/upload/product/as5.jpg" alt="IMG-BANNER" style="height: 300px;">

                        <a href="{{route('getpro')}}" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                            <div class="block1-txt-child1 flex-col-l">
                                <span class="block1-name ltext-102 trans-04 p-b-8">
                                    Laptop Lenovo
                                </span>

                                <span class="block1-info stext-102 trans-04">
                                    New 2019
                                </span>
                            </div>

                            <div class="block1-txt-child2 p-b-4 trans-05">
                                <div class="block1-link stext-101 cl0 trans-09">
                                    Shop Now
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            <div class="p-b-10">
                <h3 style="font-weight: bold;">
                    Tổng quan sản phẩm  
                </h3>
            </div>

            <div class="flex-w flex-sb-m p-b-52">
                <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                    <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
                        All Products
                    </button>
                    @foreach ($cate as $item)
                        <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".item-{{$item->id}}">
                            {{$item->name}}
                        </button>
                    @endforeach
                </div>

                <div class="flex-w flex-c-m m-tb-10">
                    <div class="home-search flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4">
                        <i class="icon-search  cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
                        <a href="{{route('getpro')}}">Search</a>
                    </div>
                </div>
            </div>

            <div class="row isotope-grid" id="changeTable">
                @foreach ($pro as $item)
                @if ($item->quantity != 0 && $item->active == "Active")
                    @if ($item->promotion_price == 0) 
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item item-{{$item->id_cate}}">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-pic hov-img0">
                                <img src="/upload/product/{{$item->image}}" alt="IMG-PRODUCT">
                                <a href="{{route('addCart', $item->id)}}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                    Thêm vào giỏ hàng
                                </a>
                            </div>
    
                            <div class="block2-txt flex-w flex-t p-t-14">
                                <div class="block2-txt-child1 flex-col-l ">
                                    <a href="{{route('detail', $item->id)}}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                            {{$item->name}}
                                    </a>
    
                                    <i class="stext-105 cl3" style="color:red">
                                            {{number_format($item->unit_price)}} <u style="">đ</u>
                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                @else 
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item item-{{$item->id_cate}}">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-pic hov-img0">
                                <img src="/upload/product/{{$item->image}}" alt="IMG-PRODUCT">
                                <span>sale</span>
                                <a href="{{route('addCart', $item->id)}}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                    Thêm vào giỏ hàng
                                </a>
                            </div>
    
                            <div class="block2-txt flex-w flex-t p-t-14">
                                <div class="block2-txt-child1 flex-col-l ">
                                    <a href="{{route('detail', $item->id)}}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                            {{$item->name}}
                                    </a>
                                    <i class="stext-105 cl3" style="color:red">
                                        {{number_format($item->promotion_price)}} <u style="">đ</u>
                                    </i>
                                    <span class="stext-105 cl3" style="color:#ccc;text-decoration: line-through;font-size:12px">
                                            {{number_format($item->unit_price)}}đ</u>
                                    </span>
                                </div>
    
                            </div>
                        </div>
                    </div>
                @endif
                
                    
                @endif
                    
                @endforeach
                
            </div>
            {{-- <div id="link">{{ $pro -> links()}}</div> --}}
        </div>
    </section>
@endsection

@section('cart')
    @if ($count == 0)
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 ">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @else 
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti " data-notify="{{$count}}">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @endif
@endsection

@section('cate')
<ul class="sub-menu">
        <li><a href="{{route('getpro')}}">Tất cả sản phẩm</a></li>
    @foreach ($cate as $item)
        <li><a href="{{route('shop', $item->id)}}">{{$item->name}}</a></li>
    @endforeach
</ul>
@endsection

@section('cus')
    @if (Auth::guard('customer')->check())
        <span style="color:#b2b2b2;font-size:13px;" class="flex-c-m trans-04 p-lr-25">
            Tài khoản: {{$cus}}
        </span>
        <a href="{{route('get-logout')}}" style="padding-top: 10px;border-right: none;">Đăng xuất</a>
    @else 
        <a style="padding-top: 10px;border: none;" href="{{route('get-customer-login')}}">Đăng nhập</a>
    @endif
@endsection
