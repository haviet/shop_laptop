@extends('customer/master')
@section('title')
    cart
@endsection

@section('content')
    <!-- breadcrumb -->
	<div class="container" style="margin-top:90px;">
            <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                <a href="{{route('home')}}" class="stext-109 cl8 hov-cl1 trans-04">
                    Home
                    <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>
    
                <span class="stext-109 cl4">
                    Shoping Cart
                </span>
            </div>
        </div>
            
    
        <!-- Shoping Cart -->
        <div class="bg0 p-t-75 p-b-85">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-12 m-lr-auto m-b-50">
                        <div class="m-l-25 m-r--38 m-lr-0-xl">
                            <div class="wrap-table-shopping-cart">
                                @if (\Session::has('mess'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{!! \Session::get('mess') !!}</li>
                                            <a style="font-size: 22px" href="{{route('generate-pdf',Session::get('id'))}}">Xem trước hóa đơn</a>
                                        </ul>
                                    </div>
                                @endif

                                @if ($count == 0)
                                    <p style="padding-left: 30px;border-top: 1px solid #ccc;padding-top: 30px;">
                                        Không có sản phẩm nào trong giỏ hàng
                                    </p>
                                @else
                                    <table class="table-shopping-cart">
                                        <tr class="table_head">
                                            <th >Hình ảnh</th>
                                            <th >Sản phẩm</th>
                                            <th >Giá tiền(VND)</th>
                                            <th >Số lượng</th>
                                            <th >Thành tiền(VND)</th>
                                            <th>Xóa</th>
                                            <th>Cập nhật</th>
                                        </tr>
                                        @foreach ($cart as $item)
                                            <tr class="table_row">
                                                <td>
                                                    <div>
                                                        <img src="/upload/product/{{$item->options->image}}" alt="IMG" style="width: 170px;">
                                                    </div>
                                                </td>
                                                <td >{{$item->name}}</td>
                                                <td >{{number_format($item->price)}}</td>
                                                <td>
                                                    @foreach ($pro as $p)
                                                        @if ($p->id == $item->id)
                                                            <input min="1" max="{{$p->quantity}}"  id-tr='{{$item->rowId}}' class="changeQuantity" type="number" value="{{$item->qty}}" style="border: 1px solid #ccc;width: 60px;text-align: center;">
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td >{{number_format($item->qty*$item->price)}}
                                                </td>
                                                <td>
                                                    <a href="{{route('del-cart', $item->rowId)}}">X</a>
                                                </td>
                                                <td id-tr='{{$item->rowId}}' style="cursor: pointer" class="update-cart">
                                                    Cập nhật
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>    
                                @endif
                            </div>
                            <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
                                <div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                    Tổng: {{number_format(str_replace(',', '',$total))}} VND
                                </div>
                            </div>
                        </div>
                    </div>
                    
                   
                    @if ($count != 0)
                        <div class="col-sm-12 col-lg-12 col-xl-12 m-lr-auto m-b-50">
                            <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                                <h4 class="mtext-109 cl2">
                                    Thanh toán:  
                                </h4>
                                <span style="margin-bottom:30px;display:block;margin-top:15px;">Vui lòng update toàn bộ sản phẩm trong giỏ hàng và điền đầy đủ thông tin trước khi thanh toán</span>
                                <!--begin::Form-->
                                <form class="m-form m-form--fit m-form--label-align-right" action="{{route('checkout')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    @foreach ($errors->all() as $error)
                                        <div class="col-lg-12 col-xl-12 alert alert-danger">{{ $error }}</div>
                                    @endforeach

                                    <div class="m-portlet__body" style="margin-top:30px">
                                        <div class="form-group m-form__group">
                                            <label>
                                                Họ tên
                                            </label>
                                            <input name="name" type="text" class="form-control m-input m-input--square">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <label>
                                                Số điện thoại
                                            </label>
                                            <input name="phone" type="number" class="form-control m-input m-input--square">
                                        </div>
                                        
                                        <div class="form-group m-form__group">
                                            <label>
                                                Địa chỉ
                                            </label>
                                            <input name="address" type="text" class="form-control m-input m-input--square">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label>Hình thức thanh toán</label>
                                        <input readonly type="text" class="form-control" name="payment" value=" Sau khi nhận hàng">
                                    </div>
                                
                                    <div class="form-group m-form__group">
                                        <label>
                                            Ghi chú
                                        </label>
                                        <textarea rows="10" name="note" type="text" class="form-control m-input m-input--square"></textarea>
                                    </div>
                                    
                                    <table style="width: 100%; text-align: center;border: 1px solid #e6e6e6;">
                                        <tbody>
                                            <tr>
                                                <th style="text-align: center">Tên sản phẩm</th>
                                                <th style="text-align: center">Số lượng</th>
                                                <th style="text-align: center">Giá</th>
                                            </tr>
                                            @foreach ($cart as $item)
                                                <tr>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->qty}}</td>
                                                    <td>{{number_format($item->qty*$item->price)}} <span style="font-size:12px">đ</span></td>
                                                </tr>
                                                
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="flex-w flex-t bor12 p-b-13" style="    margin: 20px 0px; border: none;">
                                        <div class="size-208">
                                            <span class="stext-110 cl2">
                                                Tổng thanh toán
                                            </span>
                                        </div>
            
                                        <div class="size-209">
                                            <span style="font-size:12px">đ</span>
                                            <input class="total" name="total" value="{{number_format(str_replace(',', '',$total))}}" type="text" style="background:none;display: initial;">
                                        </div>
                                    </div>
                                    <button class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                        Thanh toán
                                    </button>
                                </form>
                                <!--end::Form-->
                            </div>
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>


@endsection

@section('cart')
    @if ($count == 0)
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 ">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @else 
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti " data-notify="{{$count}}">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @endif
@endsection

@section('cate')
<ul class="sub-menu">
        <li><a href="{{route('getpro')}}">Tất cả sản phẩm</a></li>
    @foreach ($cate as $item)
        <li><a href="{{route('shop', $item->id)}}">{{$item->name}}</a></li>
    @endforeach
</ul>
@endsection



@section('cus')
    @if (Auth::guard('customer')->check())
        <span style="color:#b2b2b2;font-size:13px;" class="flex-c-m trans-04 p-lr-25">
            Tài khoản: {{$cus}}
        </span>
        <a href="{{route('get-logout')}}" style="padding-top: 10px;border-right: none;">Đăng xuất</a>
    @else 
        <a style="padding-top: 10px;border: none;" href="{{route('get-customer-login')}}">Đăng nhập</a>
    @endif
@endsection

@section('script')
    <script>
        // $('.changeQuantity').keypress(function (event) {
        //     event.preventDefault();
        // });

        $('.total').on('keydown keypress', function (event) {
            event.preventDefault();
        });

        $(document).ready(function(){
            $('.update-cart').click(function(){
                var rowId =  $(this).attr('id-tr');
                var quantity = this.parentElement.querySelector('.changeQuantity').value;                
                var url='{!!route("update-cart")!!}';
                var max =  this.parentElement.querySelector('.changeQuantity').getAttribute('max');
                if(quantity > parseInt(max)) {
                    alert('Trong kho chỉ còn lại '+max+' sản phẩm');
                } else {
                    var data = {
                        'rowId': rowId,
                        'qty': quantity,
                    };
                    $.ajax({
                        type: "get",
                        url : url,    
                        data: data,
                        dataType: 'json',
                        success: function(res) {
                            if(res.status){
                                location.reload();
                            }
                        },
                    });
                }
                
               
           }) 
           
        })   
    </script>
@endsection

