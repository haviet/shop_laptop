
@extends('customer/form')
@section('title')
    login
@endsection


@section('content')
<div class="m-stack m-stack--hor m-stack--desktop">
    <div class="m-stack__item m-stack__item--fluid">
        <div class="m-login__wrapper">
            <div class="m-login__logo">
                <a href="#">
                    <img src="assets/app/media/img//logos/logo-2.png">
                </a>
            </div>
            
            <!-- content -->
            <div class="m-login__signin">
                <div class="m-login__head">
                    <h3 class="m-login__title">
                        Đăng nhập tài khoản
                    </h3>
                </div>
                <form class="m-login__form m-form" action="{{route('post-customer-login')}}" method="post">
                    {{ csrf_field()}}
                    @if (Session::has('error'))
                        <p style="color:red; font-size:12px">{{ Session::get('error') }}</p>
                    @endif
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input" type="email" placeholder="Email" name="email" autocomplete="off">
                    </div> 
                    <div class="form-group m-form__group">
                        <input required class="form-control m-input m-login__form-input--last" type="password" placeholder="Mật khẩu" name="password">
                    </div>
                    
                    <div class="m-login__form-action">
                        <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                            Đăng nhập
                        </button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    <div class="m-stack__item m-stack__item--center">
        <div class="m-login__account">
            <span class="m-login__account-msg">
                 Bạn chưa có tài khoản? 
            </span>
            &nbsp;&nbsp;
            <a href="{{route('get-customer-register')}}" class="m-link m-link--focus m-login__account-link">
                Đăng kí
            </a>
        </div>
    </div>
</div>
@endsection
