@extends('customer/master')
@section('title')
    products
@endsection
@section('content')
    <!-- Product -->
    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140" style="margin-top: 140px;">
        <div class="container">
            <div class="p-b-10">
                <h3 style="font-weight: bold">
                    Tất cả sản phẩm
                </h3>
            </div>

            <div class="flex-w flex-sb-m p-b-52">
                <div class="flex-w flex-c-m m-tb-10">
                    <div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">
                        <i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
                        <i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
                        Search
                    </div>
                </div>
                
                <!-- Search product -->
                <div class="dis-none panel-search w-full p-t-10 p-b-15">
                    <div class="bor8 dis-flex p-l-15">
                        <button class="btn-search size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
                            <i class="zmdi zmdi-search"></i>
                        </button>
                        <input name="key" id="search" class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" placeholder="Search">
                    </div>	
                </div>
            </div>

            <div class="row" id="changeTable" style="height: auto !important">
                @foreach ($pro as $item)
                    {{-- @if ($item->quantity != 0  && $item->active == "Active") --}}
                    @if ($item->promotion_price == 0) 
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item item-{{$item->id_cate}}">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-pic hov-img0">
                                <img src="/upload/product/{{$item->image}}" alt="IMG-PRODUCT">
                                <a href="{{route('addCart', $item->id)}}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                    Thêm vào giỏ hàng
                                </a>
                            </div>
    
                            <div class="block2-txt flex-w flex-t p-t-14">
                                <div class="block2-txt-child1 flex-col-l ">
                                    <a href="{{route('detail', $item->id)}}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                            {{$item->name}}
                                    </a>
    
                                    <i class="stext-105 cl3" style="color:red">
                                            {{number_format($item->unit_price)}} <u style="">đ</u>
                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                @else 
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item item-{{$item->id_cate}}">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-pic hov-img0">
                                <img src="/upload/product/{{$item->image}}" alt="IMG-PRODUCT">
                                <span>sale</span>
                                <a href="{{route('addCart', $item->id)}}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                    Thêm vào giỏ hàng
                                </a>
                            </div>
    
                            <div class="block2-txt flex-w flex-t p-t-14">
                                <div class="block2-txt-child1 flex-col-l ">
                                    <a href="{{route('detail', $item->id)}}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                            {{$item->name}}
                                    </a>
    
                                    <i class="stext-105 cl3" style="color:red">
                                            {{number_format($item->promotion_price)}} <u style="">đ</u>
                                    </i>
                                    <span class="stext-105 cl3" style="color:#ccc;text-decoration: line-through;font-size:12px">
                                            {{number_format($item->unit_price)}}đ</u>
                                    </span>
                                </div>
    
                            </div>
                        </div>
                    </div>
                {{-- @endif --}}
                    @endif
                @endforeach
                <div id="link">{{ $pro -> links()}}</div>
            </div>
            
        </div>
    </section>
	
@endsection
@section('cate')
<ul class="sub-menu">
        <li><a href="{{route('getpro')}}">Tất cả sản phẩm</a></li>
    @foreach ($cate as $item)
        <li><a href="{{route('shop', $item->id)}}">{{$item->name}}</a></li>
    @endforeach
</ul>
@endsection

@section('cart')
    @if ($count == 0)
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 ">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @else 
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="{{$count}}">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @endif
@endsection
@section('cus')
    @if (Auth::guard('customer')->check())
        <span style="color:#b2b2b2;font-size:13px;" class="flex-c-m trans-04 p-lr-25">
            Tài khoản: {{$cus->username}}
        </span>
        <a href="{{route('get-logout')}}" style="padding-top: 10px;border-right: none;">Đăng xuất</a>
    @else 
        <a style="padding-top: 10px;border: none;" href="{{route('get-customer-login')}}">Đăng nhập</a>
    @endif
@endsection

@section('script')
    <script>        
        document.getElementById('search').addEventListener('keyup', function(e) {
            var url='{!!route("search-all")!!}';            
            
            axios.get(url, {
                params: {
                    key:this.value,
                }
            })
            .then(function (res) {       
                var html='';   
                for(var i = 0; i< res.data.product.length; i++) {
                    var pro = res.data.product[i]; 
                    if(pro.quantity != 0 && pro.active == "Active") {
                        if(pro.promotion_price == 0) {
                            html+='<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item">';
                            html+='    <div class="block2">';
                            html+='        <div class="block2-pic hov-img0">';
                            html+='            <img src="/upload/product/'+pro.image+'" alt="IMG-PRODUCT">';
                            html+='            <a href="http://localhostaddCart/'+pro.id+'" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">';
                            html+='                Thêm vào giỏ hàng';
                            html+='            </a>';
                            html+='        </div>';
                            html+='        <div class="block2-txt flex-w flex-t p-t-14">';
                            html+='             <div class="block2-txt-child1 flex-col-l ">';
                            html+='               <a href="http://localhostdetail/'+pro.id+'" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">'+pro.name+' </a>';
                            html+='                <i class="stext-105 cl3" style="color:red">';
                            html+='                    '+formatNumber(pro.unit_price)+' <u style="">đ</u>';
                            html+='                </i>';
                            html+='            </div>';
                            html+='        </div>';
                            html+='    </div>';
                            html+='</div>    ';  
                        } else {
                            html+='<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item">';
                            html+='    <div class="block2">';
                            html+='        <div class="block2-pic hov-img0">';
                            html+='            <img src="/upload/product/'+pro.image+'" alt="IMG-PRODUCT">';
                            html+='             <span>sale</span>';
                            html+='            <a href="" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">';
                            html+='                Thêm vào giỏ hàng';
                            html+='            </a>';
                            html+='        </div>';
                            html+='        <div class="block2-txt flex-w flex-t p-t-14">';
                            html+='             <div class="block2-txt-child1 flex-col-l ">';
                            html+='               <a href="http://localhostdetail/'+pro.id+'" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">'+pro.name+' </a>';
                            html+='                <i class="stext-105 cl3" style="color:red">'+formatNumber(pro.unit_price)+' <u style="">đ</u></i>';
                            html+='                 <span class="stext-105 cl3" style="color:#ccc;text-decoration: line-through;font-size:12px">';
                            html+='                        '+formatNumber(pro.promotion_price)+'đ</u>';
                            html+='                 </span>';
                            html+='            </div>';
                            html+='        </div>';
                            html+='    </div>';
                            html+='</div>    ';  
                        
                        }
                    }
                    
                }
                document.getElementById('changeTable').innerHTML = html;   
            })
            .catch(function (error) {
                console.log(error);
            });
        })
        
        function formatNumber(number) {
            num = number.toLocaleString('vi', { currency : 'VND'});
            return num;
        }
    </script>
@endsection
