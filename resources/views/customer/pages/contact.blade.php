
@extends('customer/master')
@section('content')
<style>
    .wrap-menu-desktop {
    position: initial;
    
}
</style>
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('/images/bg-01.jpg')" >
        <h2 class="ltext-105 cl0 txt-center" >
            Contact
        </h2>
    </section>
    <section class="bg0 p-t-104 p-b-116" >
		<div class="container">
			<div class="flex-w flex-tr">
				
				<div class="size-210 bor10 flex-w flex-col-m p-lr-93 p-tb-30 p-lr-15-lg w-full-md" >
					<div class="flex-w w-full p-b-42">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-map-marker"></span>
						</span>

						<div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Address
							</span>

							<p class="stext-115 cl6 size-213 p-t-18">
								Coza Store Center 8th floor, 379 Hudson St, New York, NY 10018 US
							</p>
						</div>
					</div>

					<div class="flex-w w-full p-b-42">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-phone-handset"></span>
						</span>

						<div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Lets Talk
							</span>

							<p class="stext-115 cl1 size-213 p-t-18">
								+1 800 1236879
							</p>
						</div>
					</div>

					<div class="flex-w w-full">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-envelope"></span>
						</span>

						<div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Sale Support
							</span>

							<p class="stext-115 cl1 size-213 p-t-18">
								contact@example.com
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('cate')
<ul class="sub-menu">
        <li><a href="{{route('getpro')}}">Tất cả sản phẩm</a></li>
    @foreach ($cate as $item)
        <li><a href="{{route('shop', $item->id)}}">{{$item->name}}</a></li>
    @endforeach
</ul>
@endsection

@section('cart')
    @if ($count == 0)
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 ">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @else 
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="{{$count}}">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @endif
@endsection
@section('cus')
    @if (Auth::guard('customer')->check())
        <span style="color:#b2b2b2;font-size:13px;" class="flex-c-m trans-04 p-lr-25">
            Tài khoản: {{$cus->username}}
        </span>
        <a href="{{route('get-logout')}}" style="padding-top: 10px;border-right: none;">Đăng xuất</a>
    @else 
        <a style="padding-top: 10px;border: none;" href="{{route('get-customer-login')}}">Đăng nhập</a>
    @endif
@endsection