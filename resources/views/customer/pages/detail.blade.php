@extends('customer/master')
@section('title')
    home
@endsection
@section('content')
	<div class="container" style="margin-top: 100px;">
		<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
			<div class="row">
				<div class="col-md-5 col-lg-5 p-b-30">
					<div class="p-l-25 p-r-30 p-lr-0-lg">
						<div class="wrap-slick3 flex-sb flex-w">
							<img style="width: 415px;
							object-fit: cover;" src="/upload/product/{{$pro->image}}" alt="IMG-PRODUCT">

						</div>
					</div>
				</div>
				
				<div class="col-md-7 col-lg-7 p-b-30">
					<div class="p-r-50 p-t-5 p-lr-0-lg">
						<h4 class="mtext-105 cl2 js-name-detail p-b-14">
							{{$pro->name}}
						</h4>

						<span class="mtext-106 cl2">
								{{number_format($pro->unit_price)}} VND
						</span>
						<div class="p-t-33">
							<div class="flex-w flex-r-m p-b-10" style="margin-left: -100px;">
								<div class="size-204 flex-w flex-m respon6-next" >
									<button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
										<a style="color:#fff" href="{{route('addCart', $pro->id)}}">Add to cart</a>	
									</button>
								</div>
							</div>	
						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<p class="stext-102 cl3 p-t-23">
					{!! $pro->description !!}
			</p>
			</div>
		</div>
		@if (Auth::guard('customer')->check())
			<div class="wp-cmt" id="{{$pro->id}} ">
				<strong style="margin-left: 15px">Bình luận</strong>
				<div class="comment" id="comment">
					@foreach ($cmt as $item)
						<div class="blog">
							<div class="row">
								<div class="col-md-1">
									<img class="img-cmt" src="images/JARE.jpg" alt="">
								</div>
								<div class="col-md-11">
									<div class="content">
										<div class="name-cmt">
											<strong>
												@foreach ($nameCmt as $n)
													@if ($item->id_cus == $n->id)
														{{$n->username}}
													@endif
												@endforeach
											</strong>
										</div>
										<div class="content-cmt">
											{{$item->content}}
										</div>
									</div>
									<div class="social">
										<span>Like</span>
										<span>Reply</span>
										<span>{{$item->created_at}}</span>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					
				</div>
				<div class="comment-box">
					<div class="row">
						<div class="col-md-1">
							<img  class="img-cmt" src="images/JARE.jpg" alt="">
						</div>
						<div class="col-md-11">
							<form action="{{route('post-cmt')}}" method="POST">
								{{ csrf_field() }}
								<input type="hidden"  value="{{$cus->id}}" name="cus">
								<input  type="hidden" value="{{$pro->id}}" name="pro">
								<input value="" type="text" name='content' placeholder="enter comment...">
								<button type="submit">Comment</button>
							</form>
						</div>
					</div>
					
				</div> 
				
			</div>
			
		@endif
		
	</div>
		<h3 class="pro-relate" style="margin-top: 70px;"><u>Sản phẩm liên quan</u></h3>
		<div class="col-md-12" style="margin-bottom: 70px;">
			<div class="owl-carousel owl-theme">
				@foreach ($relate as $item)
					<div class="item" >
						<a href="{{route('detail', $item->id)}}">
							<img src="/upload/product/{{$item->image}}" alt="IMG-PRODUCT">
							<p style="text-align: center;color: #333;font-size: 12px;">{{$item->name}}</p>
						</a>
						<p style="color:red; font-size:12px; text-align:center">{{number_format($item->unit_price)}} đ</p>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<style>
		.pro-relate {
			margin-top: 70px;
			text-align: center;
			margin-bottom: 50px;
			font-weight: bold;
			text-decoration: none !important;
		}
	</style>
@endsection

@section('cate')
<ul class="sub-menu">
        <li><a href="{{route('getpro')}}">Tất cả sản phẩm</a></li>
    @foreach ($cate as $item)
        <li><a href="{{route('shop', $item->id)}}">{{$item->name}}</a></li>
    @endforeach
</ul>
@endsection

@section('cart')
    @if ($count == 0)
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 ">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @else 
        <div class="icon-header-item icon-cart cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="{{$count}}">
            {{-- js-show-cart --}}
            <a href="{{route('cart')}}"><i class="zmdi zmdi-shopping-cart"></i></a>
        </div>
    @endif
@endsection
@section('cus')
    @if (Auth::guard('customer')->check())
        <span style="color:#b2b2b2;font-size:13px;" class="flex-c-m trans-04 p-lr-25">
            Tài khoản: {{$cus->username}}
        </span>
        <a href="{{route('get-logout')}}" style="padding-top: 10px;border-right: none;">Đăng xuất</a>
    @else 
        <a style="padding-top: 10px;border: none;" href="{{route('get-customer-login')}}">Đăng nhập</a>
    @endif
@endsection
@section('script')
	<script>
		$('.owl-carousel').owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:4
				}
			}
		})


		
		// function getCmt() {
		// 	var idpro = $('.wp-cmt').attr('id');
		// 	var data={
        //             'id':idpro
        //         };
        //         var url= "{!! route('cmt') !!}";
		// 	$.ajax({
		// 		type: "GET",
		// 		url: url,
		// 		data: data,
		// 		dataType: 'json',
		// 		beforeSend: function() {
		// 		},
		// 		success: function(res, status, jqXHR) {
		// 			renderCmt(res)
		// 		},
		// 	});
		// }

		// function renderCmt(data) {
		// 	var html = "";   
		// 	for(var i = 0; i<data.cmt.length; i++) {
				
		// 		html += '<div class="blog">';
		// 		html += '	<div class="row">';
		// 		html += '		<div class="col-md-1">';
		// 		html += '			<img class="img-cmt" src="images/JARE.jpg" alt="">';
		// 		html += '		</div>';
		// 		html += '		<div class="col-md-11">';
		// 		html += '			<div class="content">';
		// 		html += '				<div class="name-cmt">';
		// 		html += '					<strong>';
		// 			for(var j =0; j<data.name.length; j++) {
		// 				if(data.name[j].id == data.cmt[i].id) {
		// 					html += data.name[j].username;						
		// 				}
		// 			}
		// 		html += '					</strong>';
		// 		html += '				</div>';
		// 		html += '				<div class="content-cmt">';
		// 		html += 					data.cmt[i].content;
		// 		html += '				</div>';
		// 		html += '			</div>';
		// 		html += '			<div class="social">';
		// 		html += '				<span>Like</span>';
		// 		html += '				<span>Reply</span>';
		// 		html += '				<span>{{$item->created_at}}</span>';
		// 		html += '			</div>';
		// 		html += '		</div>';
		// 		html += '	</div>';
		// 		html += '</div>';
		// 	}
		// 	$('#comment').html(html);
		// }
		// getCmt();
	</script>
@endsection