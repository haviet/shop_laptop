<?php
//admin
// php artisan serve
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\slide;
use App\User;
use App\products;
use App\customers;
use App\category;
use App\bill;
use App\bill_detail;
use App\comment;
use Validator;
use Hash;
use Cart;   
use DB;
use Auth;
use PDF;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   
    public function index() {   
        $cate = category::all()->count();
        $pro = products::all()->count();
        $cus = customers::all()->count();
        $bill = bill::all()->count();
        $cmt = comment::all()->count();
        $detail = bill_detail::all()->count();
        $slide = slide::all()->count();
        $pro_qty = products::select('quantity')->get();
        $sum = 0;
        foreach ($pro_qty as $value) {
            $a = $value->quantity;
            $sum = $a + $sum;
            
        }
        $detail_qty = bill_detail::select('quantity')->get();
        $sum2 = 0;
        foreach ($detail_qty as $value) {
            $a = $value->quantity;
            $sum2 = $a + $sum2;
        }
        $store = $sum - $sum2;
        $result = [];
        $stt = 1;
        $total = 0;
        $start = date("Y-m-d");
        $end = date("Y-m-d").' 23:59:59';
        return view('admin.pages.home', compact('start','end','total','result','cate','pro','cus','bill','cmt','detail','slide','sum', 'sum2', 'store')); 
    }

    public function search(request $req) {
        $end = '';
        if(!$req->startDate) {
            $start = date("Y-m-d");
        } else {
            $start = $req->startDate;
        }
        if(!$req->endDate) {
            $end = date("Y-m-d").' 23:59:59';
        } else {
            $end = $req->endDate.' 23:59:59';
        }
        $bill = bill_detail::select("bill_details.*")
            ->whereBetween('created_at', [$req->startDate, $end])
            ->get();
        
        $group = array();

        foreach ( $bill as $value ) {
            $group[$value['id_product']][] = $value;
        }
        $arr = [];
        foreach($group as $value) {
            if( count($value) > 1) {
                $quantity = 0;
                $total_price = 0;
                foreach( $value as $subValue) {
                    $quantity += $subValue->quantity;
                    $total_price += $subValue->total_price;
                }
                array_push($arr, (object)[
                    'id_product' => $value[0]->id_product,
                    'quantity' => $quantity,
                    'total_price' => $total_price,

                ]);
            } else {
                array_push($arr, $value[0]);
            }
        }
        $items = [];
        foreach($arr as $item) {
            $product = products::where('id', '=', $item->id_product)->get();
            array_push($items, (object)[
                    'id_product' => $item->id_product,
                    'quantity' =>  $item->quantity,
                    'total_price' =>  $item->total_price,
                ]);
        }
        $result = [];
        $total = 0;
        foreach($items as $item) {
            $product = products::where('id', '=', $item->id_product)->get();
            array_push($result, (object)[
                'id_product' => $item->id_product,
                'productCode' => $product[0]->productCode,
                'name' =>  $product[0]->name,
                'quantity' =>  $item->quantity,
                'total_price' =>  $item->total_price,
            ]);
            $total += $item->total_price;
        }

        //
        $cate = category::all()->count();
        $pro = products::all()->count();
        $cus = customers::all()->count();
        $bill = bill::all()->count();
        $cmt = comment::all()->count();
        $detail = bill_detail::all()->count();
        $slide = slide::all()->count();
        $pro_qty = products::select('quantity')->get();
        $sum = 0;
        foreach ($pro_qty as $value) {
            $a = $value->quantity;
            $sum = $a + $sum;
            
        }
        $detail_qty = bill_detail::select('quantity')->get();
        $sum2 = 0;
        foreach ($detail_qty as $value) {
            $a = $value->quantity;
            $sum2 = $a + $sum2;
        }
        $store = $sum - $sum2;
        $stt = 1;
        return view('admin.pages.home', compact('start','end', 'total', 'stt','result', 'cate','pro','cus','bill','cmt','detail','slide','sum', 'sum2', 'store')); 
        
    }

    public function getError() {
        return view('error');
    }


    public function getCate() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cate = category::paginate(5);
            $cate_count = category::all()->count();
            $stt = 1;
            return view('admin.pages.category.category', compact('cate', 'stt', 'cate_count'));
        }
    }

    public function getAddCate() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.category.add');
        }
    }

    public function getEditCate($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        }
        $cate = category::find($id);
        if(!$cate) {
            return view('error');
        } 
        return view('admin.pages.category.edit', compact('cate'));
    }


    public function postCate(request $req) {
        $cate = new category;
        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cate->active = $req->active;
                    $cate->name = $req->name;
                    $cate->save();
                    return redirect('admin/category')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/category');
                break;
        }
    }


    public function updateCate(Request $req, $id) {
        $cate = category::find($id);

        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cate->name = $req ->name;
                    $cate->active = $req->active;
                    $cate->save();
                    return redirect('admin/category')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/category');
                break;
        }
    }

    public function deleteCate($id) {
        $cate = category::find($id);
        $cate->delete();
        return redirect() -> back(); 
    }






    // slide
    public function getSlide() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $slide = slide::paginate(5);
            $slide_count = slide::all()->count();
            $stt = 1;
            return view('admin.pages.slide.slide', compact('slide', 'slide_count', 'stt'));
        }
    }

    public function getAddSlide() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.slide.add');
        }
    }

    public function getEditSlide($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $slide = slide::find($id);
            if(!$slide) {
                return view('error');
            } 
            return view('admin.pages.slide.edit', compact('slide'));
        }
       
    }

    public function postSlide(request $req) {
        $slide = new slide;

        $messages = [
            'required.image' => 'Trường Hình ảnh không được bỏ trống',
		    'required.link' => 'Trường Link ảnh không được bỏ trống',
            'mimes'  => 'File không đúng định dạng',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => 'required| mimes:jpeg,jpg,png,gif',
            'link'     => 'required',
        ], $messages);


        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $file = $req->file('image');
                    // get name file
                    $file_name = $file -> getClientOriginalName('image');
                    // save image to upload/slide
                    $file->move('upload/slide', $file_name);
                    $slide->image = $file_name;
                    $slide->active = $req->active;
                    $slide->link = $req->link;
                    $slide->save();
                    return redirect('admin/slide')->with('success','cập nhật thành công');
                }
                break;

            case 'cancel':
                return redirect('admin/slide');
                break;
        }
    }

    public function updateSlide(Request $req, $id) {
        $slide = slide::find($id);

        $messages = [
		    'required' => ':Attribute require',
            'mimes'  => 'File is not valid',
        ];
        
        $validator = Validator::make($req->all(), [
            'image' => 'mimes:jpeg,jpg,png,gif',
            'link'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    if( $req -> hasFile('image')) {
                        $file = $req->file('image');
                        $file_name = $file -> getClientOriginalName('image');
                        $file->move('upload/slide', $file_name);
                        $slide->image = $file_name;
                        $slide->active = $req->active;
                        $slide->link = $req->link;
                        $slide->save();
                        return redirect('admin/slide')->with('success','cập nhật thành công');
                    } else {
                        $slide ->image =  $slide ->image;
                        $slide->link = $req ->link;
                        $slide->active = $req->active;
                        $slide->save();
                        return redirect('admin/slide')->with('success','cập nhật thành công');
                    }
                }
                break;

            case 'cancel':
                return redirect('admin/slide');
                break;
        }
    }

    public function deleteSlide($id) {
        $slide = slide::find($id);
        $slide->delete();
        return redirect() -> back(); 
    }

    








    // products
    public function getProduct() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $pro = products::paginate(5);
            $pro_count = products::all()->count();
            $stt = 1;
            $cate = category::all();
            return view('admin.pages.products.products', compact('pro', 'stt', 'cate', 'pro_count'));
        }
    }


    public function getAjax(Request $request, $type){
        switch ($type) {
            case 'products':
                return Datatables::of(products::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="/admin/product/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="/admin/product/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-pro-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<a>'.$val->productCode.'</a>';
                })
                ->editColumn('name', function ($val) {
                    return '<a>'.$val->name.'</a>';
                })
            
                ->editColumn('image', function ($val) {
                    return '<img style="width: 100px; height: 80px" src="/upload/product/'.$val->image.'" />';
                })
                
                ->editColumn('unit_price', function ($val) {
                    return '<a>'.number_format($val->unit_price).'</a>';
                })
                
                ->editColumn('quantity', function ($val) {
                    return '<a>'.$val->quantity.'</a>';
                })
                ->editColumn('active', function ($val) {
                    return '<a>'.$val->active.'</a>';
                })
                // ->rawColumns(['manipulation','quantity','id','name','image','unit_price', 'promotion_price', 'active'])
                ->rawColumns(['manipulation','quantity','id','name','image','unit_price', 'active'])
                ->make(true);
                break;

            case 'createBill':
                return Datatables::of(products::query())
                ->addColumn('manipulation', function ($val) {
                    if($val->active == 'Active' && $val->quantity > 0) {
                        $url = route('add-cart', $val->id);
                        return '
                        <a href='.$url.' class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Thêm ">
                        <i class="fas fa-cart-plus"></i></a>';
                    }
                })
                ->editColumn('productCode', function ($val) {
                    if($val->active == 'Active' && $val->quantity > 0) {
                        return '<a>'.$val->productCode.'</a>';
                    }
                })
                ->editColumn('name', function ($val) {
                    if($val->active == 'Active' && $val->quantity > 0) {
                        return '<a>'.$val->name.'</a>';
                    }
                })
                
                ->editColumn('unit_price', function ($val) {
                    if($val->active == 'Active' && $val->quantity > 0) {
                        return '<a>'.number_format($val->unit_price).'</a>';
                    }
                })
                
                ->editColumn('quantity', function ($val) {
                    if($val->active == 'Active' && $val->quantity > 0) {
                        return '<a>'.$val->quantity.'</a>';
                    }
                })
            
                ->rawColumns(['manipulation','productCode', 'quantity','name','unit_price'])
                ->make(true);
                break;

            case 'bills':
                return Datatables::of(bill::query())
                ->addColumn('manipulation', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            if((int)$val->employeeCode == Auth::user()->id) { 
                                return '
                                <a href="/admin/bill-details/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                                <i class="fa fa-book"></i></a>
                                
                                <a href="/admin/bill/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                                <i class="la la-edit"></i></a>
                                <a id="del-bill-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                                    <i class="la la-trash"></i>
                                </a>';
                            } 
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '
                            <a href="/admin/bill-details/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                            <i class="fa fa-book"></i></a>
                            
                            <a href="/admin/bill/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                            <i class="la la-edit"></i></a>
                            <a id="del-bill-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                                <i class="la la-trash"></i>
                            </a>';
                    } 
                    
                })
                ->editColumn('id', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->id.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->id.'</a>';
                    } 
                })
                ->editColumn('id_customer', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->id_customer.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->id_customer.'</a>';
                    } 
                
                })
                ->editColumn('status', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->status.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        $url = route('transform-status', $val->id);
                        $auth = csrf_field();
                        if($val->status == 'Chờ tiếp nhận') {
                            return '<form action='. $url.' method="post">
                            '.$auth.'
                            <button style="cursor: pointer">'.$val->status.'</button>
                            </form>';

                        } else {
                            return '<a>'.$val->status.'</a>';
                        }
                       
                    } 
                
                })
            
                ->editColumn('username', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->username.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->username.'</a>';
                    } 
                })
                ->editColumn('employeeCode', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->employeeCode.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->employeeCode.'</a>';
                    } 
                })
                ->editColumn('employeeName', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->employeeName.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->employeeName.'</a>';
                    } 
                })
                ->editColumn('phone', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->phone.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->phone.'</a>';
                    } 
                })
                ->editColumn('address', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->address.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->address.'</a>';
                    } 
                })
                ->editColumn('date_order', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->date_order.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->date_order.'</a>';
                    } 
                })
                
                ->editColumn('total', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->total.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->total.'</a>';
                    } 
                })

                ->editColumn('payment', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->payment.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->payment.'</a>';
                    } 
                })
                ->editColumn('note', function ($val) {
                    if(Auth::user()->position == "Nhân viên") {
                        if((int)$val->employeeCode == Auth::user()->id) { 
                            return '<a>'.$val->note.'</a>';
                        } 
                    }
                    if(Auth::user()->position == "Quản trị viên") {
                        return '<a>'.$val->note.'</a>';
                    } 
                })
                ->rawColumns(['status','manipulation','employeeCode','employeeName', 'id', 'id_customer', 'username','phone','address','date_order', 'total','payment', 'note'])
                ->make(true);
                break;


            case 'cus':
                return Datatables::of(customers::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="/admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="/admin/customer/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-cus-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('username', function ($val) {
                    return '<p>'.$val->username.'</p>';
                })
            
                ->editColumn('email', function ($val) {
                    return '<p>'.$val->email.'</p>';                
                })
            
                ->rawColumns(['manipulation', 'id', 'username','email' ])
                ->make(true);
                break;
            case 'detail':
                return Datatables::of(bill_detail::query())
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('id_bill', function ($val) {
                    return '<p>'.$val->id_bill.'</p>';
                })

                ->editColumn('id_product', function ($val) {
                    $pro = products::find($val->id_product);
                    return '<p>'.$pro->productCode.'</p>';
                })

                ->editColumn('name_pro', function ($val) {
                    $pro = products::find($val->id_product);
                    return '<p>'.$pro->name.'</p>';
                })
            
                ->editColumn('quantity', function ($val) {
                    return '<p>'.$val->quantity.'</p>';
                })
            
                ->editColumn('unit_price', function ($val) {
                    return '<p>'.number_format($val->unit_price).'</p>';
                })
            
                ->editColumn('total_price', function ($val) {
                    return '<p>'.number_format($val->total_price).'</p>';                
                })
            
                ->rawColumns(['id', 'id_bill','name_pro','email','id_product', 'quantity', 'unit_price', 'total_price'])
                ->make(true);
                break;
            case 'cate':
                return Datatables::of(category::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="/admin/category/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="/admin/category/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-cate-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('name', function ($val) {
                    return '<p>'.$val->name.'</p>';
                })
                ->editColumn('active', function ($val) {
                    return '<p>'.$val->active.'</p>';
                })
            
                ->rawColumns(['id', 'name','active','manipulation' ])
                ->make(true);
                break;

            case 'user':
                return Datatables::of(user::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a href="/admin/user/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Xem chi tiết">
                    <i class="fa fa-book"></i></a>
                    
                    <a href="/admin/user/edit/'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                    <i class="la la-edit"></i></a>
                    <a id="del-user-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('name', function ($val) {
                    return '<p>'.$val->name.'</p>';
                })
                ->editColumn('email', function ($val) {
                    return '<p>'.$val->email.'</p>';
                })

                ->editColumn('position', function ($val) {
                    return '<p>'.$val->position.'</p>';
                })
            
                ->rawColumns(['id', 'name','email','manipulation', 'position' ])
                ->make(true);
                break;


            case 'cmt':
                return Datatables::of(comment::query())
                ->addColumn('manipulation', function ($val) {
                    return '
                    <a id="del-cmt-'.$val->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                        <i class="la la-trash"></i>
                    </a>';
                })
                ->editColumn('id', function ($val) {
                    return '<p>'.$val->id.'</p>';
                })
                ->editColumn('id_cus', function ($val) {
                    return '<p>'.$val->id_cus.'</p>';
                })
                ->editColumn('id_pro', function ($val) {
                    return '<p>'.$val->id_pro.'</p>';
                })

                ->editColumn('content', function ($val) {
                    return '<p>'.$val->content.'</p>';
                })
            
                ->rawColumns(['id', 'id_cus','id_pro','manipulation', 'content' ])
                ->make(true);
                break;
            default:
                break;
        }
       
    }

    public function getAjaxDel(Request $request, $type) {
        switch ($type) {
            case 'products':
                $id = $request->get('id');
                products::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'bills':
                $id = $request->get('id');
                bill::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            
            case 'cate':
                $id = $request->get('id');
                category::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;

            case 'user':
                $id = $request->get('id');
                user::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'cus':
                $id = $request->get('id');
                customers::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            case 'cmt':
                $id = $request->get('id');
                comment::where('id',$id)->delete();
                return response([
                    'status' => true,
                ]);
                break;
            default:
                break;
        }
       
    }
    public function getAddProduct() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cate = category::all();
            return view('admin.pages.products.add', compact('cate'));
        }
        
    }

    public function getEditProduct($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $pro = products::find($id);
            $cate = category::all();
            if(!$pro) {
                return view('error');
            } 
            return view('admin.pages.products.edit', compact('pro', 'cate'));
        }
       
    }

    public function postProduct(request $req) {
        $messages = [
            'required.name' => 'Tên sản phẩm không được để trống',
		    'required.unit_price' => 'Giá sản phẩm không được để trống',
		    'required.image' => 'Hình ảnh không được để trống',
		    'required.quantity' => 'Số lượng không được để trống',
		    'required.description' => 'Mô tả không được để trống',
		    'required.productCode' => 'Mã sản phẩm không được để trống',
            'mimes'  => 'File không đúng định dạng',
        ];
        $validator = Validator::make($req->all(), [
            'image' => 'required| mimes:jpeg,jpg,png,gif',
            'unit_price'     => 'required',
            'name'     => 'required',
            'quantity'     => 'required',
            'description'     => 'required',
            'productCode'     => 'required',
        ], $messages);
        switch ($req->input('action')) {
            case 'add':
                $proOld = products::where('productCode', '=', $req->productCode)->get();
                // if old product is not exist
                if($proOld->count() == 0) {
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator);
                    } else {
                        $pro = new products;
                        $file = $req->file('image');
                        $file_name = $file -> getClientOriginalName('image');
                        $file->move('upload/product', $file_name);
                        $pro->image = $file_name;
                        $pro->active = $req->active;
                        $pro->productCode = $req->productCode;
                        $pro->id_cate = $req->input('id_cate');
                        $pro->name = $req->name;
                        $pro->unit_price = $req->unit_price;
                        $pro->promotion_price = 0;
                        // $pro->promotion_price = $req->promotion_price;
                        $pro->quantity = $req->quantity;
                        $pro->description = $req->description;
    
                        $pro->save();
                        return redirect('admin/product')->with('success','Thêm mới thành công');
                    }
                } else {
                    return redirect()->back()->withErrors('Mã sản phẩm đã tồn tại');
                }
                break;
            case 'cancel':
                return redirect('admin/product');
                break;
        }
    }


    public function updateProduct(Request $req,$id) {
        $messages = [
            'required.name' => 'Tên sản phẩm không được để trống',
		    'required.unit_price' => 'Giá sản phẩm không được để trống',
		    'required.quantity' => 'Số lượng không được để trống',
		    'required.description' => 'Mô tả không được để trống',
		    'required.productCode' => 'Mã sản phẩm không được để trống',
            'mimes'  => 'File không đúng định dạng',
        ];
        $validator = Validator::make($req->all(), [
            'image' => 'mimes:jpeg,jpg,png,gif',
            'unit_price'     => 'required',
            'name'     => 'required',
            'quantity'     => 'required',
            'description'     => 'required',
            'productCode'     => 'required',
        ], $messages);
        switch ($req->input('action')) {
            case 'save':
                // find out productCode exist or not
                $proOld = products::where('productCode', '=', $req->productCode)->get();
                // if productCode does not exist, update with $id
                // or if  productCode exists and same id,  update with $id
                if($proOld->count() == 0 || ($proOld->count() > 0 && $proOld[0]->id == (int)$id)) { 
                    $pro = products::find($id);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator);
                    } else {
                        if( $req -> hasFile('image')) {
                            $file = $req->file('image');
                            $file_name = $file -> getClientOriginalName('image');
                            $file->move('upload/product', $file_name);
                            $pro->image = $file_name;
                            $pro->productCode = $req->productCode;
                            $pro->active = $req->active;
                            $pro->id_cate = $req->input('id_cate');
                            $pro->name = $req->name;
                            $pro->unit_price = $req->unit_price;
                            $pro->promotion_price = 0;
                            $pro->quantity = $req->quantity;
                            $pro->description = $req->description;
                            $pro->save();
                            return redirect('admin/product')->with('success','cập nhật thành công');
                        } else {
                            $pro->image =  $pro->image;
                            $pro->active = $req->active;
                            $pro->productCode = $req->productCode;
                            $pro->id_cate = $req->input('id_cate');
                            $pro->name = $req->name;
                            $pro->unit_price = $req->unit_price;
                            $pro->promotion_price = 0;
                            $pro->quantity = $req->quantity;
                            $pro->description = $req->description;
                            $pro->save();
                            return redirect('admin/product')->with('success','cập nhật thành công');
                        }
                    }
                    break;
                } 
                // if productCode exists but differs id
                if($proOld->count() > 0 && $proOld[0]->id !== (int)$id) {
                    $pro = products::find($proOld[0]->id);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator);
                    } else {
                        if( $req -> hasFile('image')) {
                            $file = $req->file('image');
                            $file_name = $file -> getClientOriginalName('image');
                            $file->move('upload/product', $file_name);
                            $pro->image = $file_name;
                            $pro->productCode = $req->productCode;
                            $pro->active = $req->active;
                            $pro->id_cate = $req->input('id_cate');
                            $pro->name = $req->name;
                            $pro->unit_price = $req->unit_price;
                            $pro->promotion_price = 0;
                            $pro->quantity = $proOld[0]->quantity + $req->quantity;
                            $pro->description = $req->description;
                            $pro->save();
                            $product = products::find($id);
                            $product->delete();
                            return redirect('admin/product')->with('success','cập nhật thành công');
                        } else {
                            $pro->image =  $pro->image;
                            $pro->active = $req->active;
                            $pro->productCode = $req->productCode;
                            $pro->id_cate = $req->input('id_cate');
                            $pro->name = $req->name;
                            $pro->unit_price = $req->unit_price;
                            $pro->promotion_price = 0;
                            $pro->quantity = $proOld[0]->quantity + $req->quantity;
                            $pro->description = $req->description;
                            $pro->save();
                            $product = products::find($id);
                            $product->delete();
                            return redirect('admin/product')->with('success','cập nhật thành công');
                        }
                    }
                    break;
                }                
                break;
            case 'cancel':
                return redirect('admin/product');
                break;
        }
    }


    public function deleteProduct($id) {
        $pro = products::find($id);
        $pro->delete();
        return redirect() -> back(); 
    }

   

    
    // customer
    public function getCustomer() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cus = customers::all();
            $stt = 1;
            return view('admin.pages.customer.customer', compact('cus', 'stt'));
        }
       
    }

    public function getAddCustomer() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.customer.add');
        }
        
    }

    public function getEditCustomer($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $cus = customers::find($id);
            return view('admin.pages.customer.edit', compact('cus'));
        }
       
    }

    public function postCus(request $req) {
        $cus = new customers;
        $messages = [
            'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:customers',
            'password' => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cus->email = $req->email;
                    $cus->username = $req->name;
                    $cus ->password = Hash::make($req->password);
                    $cus->save();
                    return redirect('admin/customer')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/customer');
                break;
        }
    }


    public function updateCus(request $req,$id) {
        $cus = customers::find($id);

        $messages = [
		    'name' => ':Attribute require',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'email|required|unique:customers,email,'.$cus->id,
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $cus->username = $req ->name;
                    $cus->email = $req->email;
                    $cus->save();
                    return redirect('admin/customer')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/customer');
                break;
        }
    }

    public function deleteCus($id) {
        $cus = customers::find($id);
        $cus->delete();
        return redirect() -> back(); 
    }



    

    // user
    public function getUser() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $user = User::all();
            $stt = 1;
            return view('admin.pages.users.users', compact('user', 'stt'));
        }
        
    }

    public function getAddUser() {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            return view('admin.pages.users.add');
        }
       
    }

    public function getEditUser($id) {
        if(Auth::user()->position == "Nhân viên") {
            Auth::logout();
            return redirect()->to('admin/login');
        } else {
            $user = User::find($id);
            return view('admin.pages.users.edit', compact('user'));
        }
        
    }

    public function postUser(request $req) {
        $user = new User;
        $messages = [
            'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:users',
            'password' => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'add':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $user->email = $req->email;
                    $user->name = $req->name;
                    $user->position = $req->position;
                    $user ->password = Hash::make($req->password);
                    $user->save();
                    return redirect('admin/user')->with('success','Thêm mới thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/user');
                break;
        }
    }

    public function updateUser(Request $req, $id) {
        $user = User::find($id);

        $messages = [
		    'name.required' => ':Attribute require',
            'email' => 'Email is not valid',
            'unique' => 'Email had exist',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'email'     => 'required|email|unique:users,email,'.$user->id,
        ], $messages);

        switch ($req->input('action')) {
            case 'edit':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    
                    $user->name = $req ->name;
                    $user->email = $req->email;
                    $user->position = $req->position;
                    $user->save();
                    return redirect('admin/user')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/user');
                break;
        }
    }

    // bill
    public function bill() {
        $customer = customers::all();
        $bill = bill::paginate(10);
        $bill_count = bill::all()->count();
        $stt = 1;
        return view('admin.pages.bills.bills', compact('bill', 'stt', 'bill_count', 'customer'));
    }

    public function getEditBill($id) {
        $bill = bill::find($id);
        return view('admin.pages.bills.edit', compact('bill'));
    }

    public function updateBill(Request $req, $id) {
        $bill = bill::find($id);

        $messages = [
		    'required' => ':Attribute require',
        ];
        
        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'phone'     => 'required',
            'address'     => 'required',
        ], $messages);

        switch ($req->input('action')) {
            case 'save':
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $bill->payment = $req->payment;
                    $bill->username = $req->name;
                    $bill->phone = $req->phone;
                    $bill->address = $req->address;
                    $bill->note = $req->note;
                    $bill->save();
                    return redirect('admin/bill')->with('success','cập nhật thành công');
                }
                break;
            case 'cancel':
                return redirect('admin/bill');
                break;
        }
    }

    public function transformStatus(Request $req, $id) {
        $bill = bill::find($id);
        $bill->status = 'Đã tiếp nhận';
        $bill->save();
        return redirect('admin/bill')->with('success','Tiếp nhận thành công');
    }

    public function billDetails($id) {
        $bill_detail = bill_detail::where('id_bill', '=', $id)->get();
        $stt = 1;
        $pro = products::all();
        return view('admin.pages.bills.detail', compact('bill_detail', 'stt','pro'));
    }

    public function getAddBill() {
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        $product = products::all();
        $employee = User::all();
        $total = Cart::subtotal();
        $date = date("Y-m-d");
    	return view('admin.pages.bills.add', compact('product', 'count', 'employee', 'cart','total','date'));
    }

    public function addCart($id) {
        $pro = products::find($id);
            Cart::add(['id' => $id, 
                        'name' => $pro->name, 
                        'qty' => 1, 
                        'price' => $pro->unit_price,
                        'weight' => 1,
                        'options' => ['image' =>  $pro->image]]);
            return redirect()->back();
    }

    public function deleteCart($rowId) {
        Cart::remove($rowId);
        return back();
    }

    public function checkoutAdmin(Request $req) {
        $bill = new bill;
        $cart = Cart::content();
        $messages = [
		    'name.required' => 'Tên khách hàng không được để trống',
		    'phone.required' => 'Số điện thoại không được để trống',
		    'address.required' => 'Địa chỉ không được để trống',
		    'email.required' => 'Email không được để trống',
            
        ];

        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'phone'     => 'required',
            'address'     => 'required',
            'email'     => 'required',

        ], $messages);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // $bill->id_customer = 0;
            $bill->username = $req ->name;
            $bill->phone = $req ->phone;
            $bill->address = $req ->address;
            $bill->email = $req ->email;
            $bill->date_order =  date('Y-m-d H:i:s');
            $bill->payment = $req ->payment;
            $bill->employeeCode = $req ->employeeCode;
            $bill->employeeName = $req ->employeeName;
            $bill->note = $req ->note;
            $bill->status = 'Đã tiếp nhận';
            $bill->total = $req ->total;
            $bill->save();
            foreach($cart as $item) {
                $bill_detail = new bill_detail;
                $bill_detail->id_bill = $bill->id;
                $bill_detail->id_product = $item->id;
                $bill_detail->quantity = $item->qty;
                $bill_detail->unit_price = $item->price;
                $bill_detail->total_price = $item->qty*$item->price;
                $bill_detail->save();

                $pro = products::where('id', '=' , $bill_detail->id_product)->first();
                $pro->quantity = $pro->quantity - $bill_detail->quantity;
                $pro->save();
            }
            Cart::destroy();
            return back()->with('mess', 'Tạo đơn hàng thành công. Mã đơn hàng là '.$bill->id)
                        ->with('id', $bill->id);
        }  
    }






    // bill-detail
    public function billdetail() {
        $bill_detail = bill_detail::paginate(10);
        $detail_count = bill_detail::all()->count();
        $pro = products::all();
        $stt = 1;
        return view('admin.pages.bill-detail.bill-detail', compact('bill_detail', 'stt', 'detail_count', 'pro'));
    }

    public function deleteBilldetail($id) {
        $bill_detail = bill_detail::find($id);
        $bill_detail->delete();
        return redirect() -> back(); 
    }


    // comment
    public function comment() {
        $comment = comment::paginate(10);
        $cmt_count = comment::all()->count();
        $pro = products::all();
        $cus = customers::all();
        $stt = 1;
        return view('admin.pages.comments.comment', compact('comment', 'stt', 'cmt_count', 'pro', 'cus'));
    }
}
