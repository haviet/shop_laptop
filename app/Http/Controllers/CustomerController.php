<?php

namespace App\Http\Controllers;
use App\customers;
use Illuminate\Http\Request;
use Hash;
use Auth;
use Validator;

class CustomerController extends Controller
{

    public function getRegister() {
        if (Auth::guard('customer')->check()) {
            return redirect('home');
        }
        return view('customer.pages.register');
    }

    public function postRegister(Request $req) {
        $messages = [
            'same' => 'Mật khẩu nhập lại chưa chính xác',
            'unique' => 'Email đã tồn tại',
        ];
        
        $validator = Validator::make($req->all(), [
            're_password' => 'same:password',
            'email'     => 'unique:customers',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $customer = new customers();
            $customer->email = $req->email;
            $customer->username = $req->username;
            $customer->password = Hash::make($req->password);
            $customer->save();
            return redirect('login')->with('success','Tạo tài khoản thành công');
        }
    }

    public function getLogin() {
        if (Auth::guard('customer')->check()) {
            return redirect('home');
        }
        return view('customer.pages.login');
    }

    public function postLogin(Request $request) {
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('home');
        }  else {
            return redirect()->back()->with('error', 'Tên tài khoản hoặc mật khẩu không chính xác');
        }
    }

    public function getLogout() {
        Auth::guard('customer')->logout();
          return redirect('login');
    } 
}
