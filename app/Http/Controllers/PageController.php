<?php
// template
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\slide;
use App\User;
use App\products;
use App\customers;
use App\category;
use App\bill;
use App\bill_detail;
use App\comment;
use Cart;
use Auth;
use session;
use Validator;
use DB;
use PDF;

class PageController extends Controller
{

    public function getIndex() {
        $cate = category::all();
        $slide = slide::all();
        $pro = products::all();
        Cart::count();
        $cart = Cart::content();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user()->username;
            return view('customer.pages.home', compact('cate', 'slide', 'pro', 'count', 'cus'));
        }
        return view('customer.pages.home', compact('cate', 'slide', 'pro', 'count'));
    }

    
    public function getHome() {
        $cate = category::where('active', 'Active')->get();
        $slide = slide::all();
        $pro = products::all();
        Cart::count();
        $cart = Cart::content();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user()->username;
            return view('customer.pages.home', compact('cate', 'slide', 'pro', 'count', 'cus'));
        }
        return view('customer.pages.home', compact('cate', 'slide', 'pro', 'count'));
    }

  
    public function getDetail($id) {
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        $pro = products::find($id);
        if ( !$pro) {
            return view('error');
        }
        if ($pro->quantity == 0 || $pro->active == "Inactive" ) {
            return view('error');
        }
        $cate = category::all();
        $relate = DB::table('products')->where([
            ['active', '=', 'Active'],
            ['quantity', '<>', 0],
            ['id_cate', '=', $pro->id_cate]
        ])->get();
        $cmt = comment::where('id_pro', $pro->id)->get();
        
        $cusCmt = customers::all();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user();
            $nameCmt = customers::all();
            return view('customer.pages.detail', compact('pro', 'count', 'cate', 'relate', 'cus','nameCmt', 'cmt'));
        }
        return view('customer.pages.detail', compact('pro', 'count', 'cate', 'relate', 'cmt'));
    }

    public function getCart() {
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        $total = Cart::subtotal();
        $cate = category::all();
        $pro = products::all();
        $cus = Auth::guard('customer')->user()->username;
        return view('customer.pages.cart', compact('count', 'cart', 'total', 'cate', 'cus', 'pro'));
    }

    public function addCart($id) {
        if(Auth::guard('customer')->check()) {
            $pro = products::find($id);
            Cart::add(['id' => $id, 
                        'name' => $pro->name, 
                        'qty' => 1, 
                        'price' => $pro->unit_price,
                        'weight' => 1,
                        'options' => ['image' =>  $pro->image]]);
            return redirect('cart');
        } 
        return redirect('cart');
        
    }

    public function delCart($rowId) {
        Cart::remove($rowId);
        return back();
    }
    
    public function updateCart(Request $req) {
        $rowId = $req->get('rowId');
        $qty = $req->get('qty');
        $a = Cart::update($rowId, ['qty' => $qty]);
        return response([
            'rowId'=>$rowId,
            'status'=>true,
            'qty'=>$qty,
        ]);
    }

    public function checkout(Request $req) {
        $bill = new bill;
        $cus = Auth::guard('customer')->user()->id;
        $cart = Cart::content();
        $messages = [
		    'required' => ':Attribute require',
        ];

        $validator = Validator::make($req->all(), [
            'name'     => 'required',
            'phone'     => 'required',
            'address'     => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $bill->id_customer = $cus;
            $bill->username = $req ->name;
            $bill->phone = $req ->phone;
            $bill->address = $req ->address;
            $bill->date_order =  date('Y-m-d H:i:s');
            $bill->payment = $req ->payment;
            $bill->email = $req ->email;
            $bill->payment = $req ->payment;
            $bill->note = $req ->note;
            $bill->total = $req ->total;
            $bill->status = 'Chờ tiếp nhận';
            $bill->save();
            foreach($cart as $item) {
                $bill_detail = new bill_detail;
                $bill_detail->id_bill = $bill->id;
                $bill_detail->id_product = $item->id;
                $bill_detail->quantity = $item->qty;
                $bill_detail->unit_price = $item->price;
                $bill_detail->total_price = $item->qty*$item->price;
                $bill_detail->save();

                $pro = products::where('id', '=' , $bill_detail->id_product)->first();
                $pro->quantity = $pro->quantity - $bill_detail->quantity;
                $pro->save();
            }
            Cart::destroy();
            return back()->with('mess', 'Tạo đơn hàng thành công. Đơn hàng đang trong trạng thái chờ tiếp nhận. Mã đơn hàng của quý khách là '.$bill->id)
                        ->with('id', $bill->id);
        }  
    }


    public function getShop($id) {
        $pro = DB::table('products')->where([
            ['active', '=', 'Active'],
            ['quantity', '<>', 0],
            ['id_cate', '=', $id]
        ])->paginate(8);
        $cat = category::where('id', '=', $id)->first();
        $cate = category::all();
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user();
            return view('customer.pages.shop', compact('pro', 'cate', 'cat','count', 'cus'));
        }
        return view('customer.pages.shop', compact('pro', 'cate', 'cat','count'));
    }

    public function searchShop(Request $req) {
        // $pro = DB::table('products')->where([
        //     ['id_cate', '=', $req->id],
        //     ['name', 'LIKE', '%'.$req->key.'%'],
        // ])->get();
        $pro = DB::table('products')
                    ->where('name', 'LIKE',  '%'.$req->key.'%')
                    ->orWhere('unit_price','LIKE',  '%'.$req->key.'%')
                    ->get();
        return response([
            'product'=>$pro,
        ]);
    }

    public function getpro(Request $req) {
        $pro = DB::table('products')->where([
            ['active', '=', 'Active'],
            ['quantity', '<>', 0]
        ])->paginate(8);
        $cate = category::all();
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user();
            return view('customer.pages.products', compact('pro', 'cate', 'count', 'cus'));
        }
        return view('customer.pages.products', compact('pro', 'cate', 'count'));
    }

    public function searcAll(Request $req) {
        $pro = DB::table('products')
                    ->where('name', 'LIKE',  '%'.$req->key.'%')
                    ->orWhere('unit_price','LIKE',  '%'.$req->key.'%')
                    ->get();
        return response([
            'product'=>$pro,
        ]);
    }

    public function postCmt(Request $req) {
        $comment = new comment;
        $comment->id_cus = intval($req->cus); // chuyển sang kiểu số nguyên
        $comment->id_pro = intval($req->pro);
        $comment->content = $req->content;
        
        $comment->save();
        return back();
    }

    public function contact() {
        $cate = category::all();
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user();
            return view('customer.pages.contact', compact('cus', 'cate', 'count'));
        }
        return view('customer.pages.contact', compact('cate', 'count'));
    }

    public function about() {
        $cate = category::all();
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        if(Auth::guard('customer')->check()) {
            $cus = Auth::guard('customer')->user();
            return view('customer.pages.about', compact('cus', 'cate', 'count'));
        }
        return view('customer.pages.about', compact('cate', 'count'));
    }

    public function generatePDF($id)
    {
        $bill = bill::find($id);
        if(!$bill) {
            return redirect()->back()->with('noti', 'Hóa đơn không tồn tại');
        }
            $bill_detail = bill_detail::where('id_bill', $id)->get();
            $arrName = [];
            foreach($bill_detail as $b) {
                $pro = products::find($b->id_product);
                array_push($arrName,[$pro->id, $pro->name]);
            }
            
            $name = $bill->username;
            $payment = $bill->payment;
            $note = $bill->note;
            $add = $bill->address;
    
            $data = ['name' =>$name ,
                'phone' => $bill->phone,
                'add' =>$add,
                'payment' =>$payment,
                'date' => $bill->date_order,
                'total' => $bill->total,
                'id' => $bill->id,
                'note' =>$note,
                'bill'=>$bill_detail,
                'arrName' => $arrName,
            ];
            $pdf = PDF::loadView('myPDF', $data);
            return $pdf->download('bill.pdf');
        
        
    }

    public function getCmt(Request $req) {
        $cmt = comment::where('id_pro', '=', $req->id)->get();
        $name = customers::all();
        return response([
           'cmt' => $cmt,
           'name' => $name,
        ]);
    }
}

