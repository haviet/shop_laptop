<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class CheckCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'customer')
    {
        if (!Auth::guard('customer')->check()) {
            return redirect('login');
        }
        return $next($request);
    }
}
