<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'categories';

    public function products() {
        return $this -> hasMany('App\products', 'id_cate', 'id');
    }
}
