<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('id_customer')->references('id')->on('customers')->onDelete('cascade');
            $table->integer('id_customer')->unsigned()->nullable();
            $table->string('username'); 
            $table->string('email'); 
            $table->string('employeeCode')->nullable(); 
            $table->string('employeeName')->nullable(); 
            $table->integer('phone');
            $table->string('address');
            $table->date('date_order');
            $table->string('total');
            $table->string('payment');
            $table->string('status');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
